<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html>

<html lang="en">
<head>

    <meta charset="utf-8">
    <title>ecm-portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">
    <link href="asset/css/style.css" rel="stylesheet">
    <link href="asset/css/pages/dashboard.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->


</head>
<body>

@include('layout.admin.navbarToplayout')

<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <span class="span12">
                    <div class="widget widget-nopad">
                        <div class="widget-header"> <i class="icon-list-alt"></i>
                            <h3> Stats</h3>
                        </div>
                        <!-- /widget-header -->
                        <div class="widget-content">
                            <div class="widget big-stats-container">
                                <div class="widget-content">
                                       <div id="big_stats" class="cf">
                                        <div class="stat">Consignment Releases <i style="color: darkred" class="icon-anchor"></i> <span class="value">{{$noconsignments}}</span> </div>
                                        <!-- .stat -->

                                        <div class="stat">Bank <i style="color: darkred" class="icon-thumbs-up-alt"></i> <span class="value">{{$nobanks}}</span> </div>
                                        <!-- .stat -->

                                        <div class="stat">Bdc <i style="color: darkred" class="icon-twitter-sign"></i> <span class="value">{{$nobdcs}}</span> </div>
                                        <!-- .stat -->

                                        <div class="stat">Deport <i style="color: darkred" class="icon-bullhorn"></i> <span class="value">{{$nodepots}}</span> </div>
                                        <!-- .stat -->
                                    </div>
                                </div>
                                <!-- /widget-content -->

                            </div>
                        </div>
                    </div>
                    </span>
            </div>


            <div class="row">
                <span class="span12">
                    <div class="widget widget-table action-table">
                        <div class="widget-header"> <i class="icon-th-list"></i>
                            <h3>Rep Records Today <b>{{date('Y-m-d')}}</b></h3>
                        </div>
                        <!-- /widget-header -->
                        <div class="widget-content">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>FieldRep Name</th>
                                    <th>Consignment </th>
                                    <th>Total Quantity</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($depouserrecord as $d)
                                <tr>
                                    <td>{{$d->name}}</td>
                                    <td>{{$d->consignmentname}} </td>
                                    <td>{{$d->number}} </td>
                                </tr>
                               @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /widget-content -->
                    </div>
                 </span>
            </div>
           </div>
           </div>
</div>
<!-- /main -->

<!-- /extra -->
{{--{{include('include/footer')}}--}}
        <!-- /footer -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="asset/js/jquery-1.7.2.min.js"></script>
<script src="asset/js/excanvas.min.js"></script>
<script src="asset/js/chart.min.js" type="text/javascript"></script>
<script src="asset/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="asset/js/full-calendar/fullcalendar.min.js"></script>

<script src="asset/js/base.js"></script>


</body>
</html>
