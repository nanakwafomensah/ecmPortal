<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use PDF;
use DB;

class PdfController extends Controller
{
    //for the Administrator
    public function dailyreport(Request $request){
        $pdfname=null;
        $fromDate=$request->startdate;
        $toDate=$request->enddate;
        $typereport=$request->typereport;
        $bdc=$request->bdc;
        $depotname=$request->depotname;
        $fieldrep=trim($request->fieldrep);
        $supervisor=trim($request->supervisor);
        
   
        $depotlocation= DB::table('depots')->select([DB::raw('location')])->where('name',$depotname)->first();
        $bank=$request->bank;
        $product=DB::table('consignments')->select([DB::raw('product')])->where('consignmentname',$request->consignmentname)->first();
        $grossMetricTonInAir=DB::table('consignments')->select([DB::raw('totalquantity')])->where('consignmentname',$request->consignmentname)->first();


        $repContact=DB::table('users')->select([DB::raw('tel')])->where('name',$fieldrep)->first();
        $supContact=DB::table('users')->select([DB::raw('tel')])->where('name',$supervisor)->first();
        
        if($typereport == "1"){//DAILY REPORT
            $pdfname='Daily_'.$depotname.'_'.$product->product.'_'.date('Y-M-d');
            $dailyrecord =  DB::table('deportdatas')->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('depotname',$depotname)->where('bank',$bank)->where('bdc',$bdc)->where('user',$fieldrep)->where('status','valid')->get();
            $summarysheet= DB::table('deportdatas')
                ->select([DB::raw('COUNT(*) as number,customer,SUM(obslitre) as obslitre,SUM(litre) as litre,SUM(vac) as vac,SUM(tonmetricair) as tonmetricair')])
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                ->where('supervisor',$supervisor)->where('status','valid')
                ->groupBy('customer')
                ->orderBy('obslitre','asc')
                ->get();
                
                
//                
//                DB::table('deportdatas')
//                ->join('consignments', 'deportdatas.consignmentname', '=', 'consignments.consignmentname')
//                ->select(DB::raw('count(deportdatas.customer) as number'),'deportdatas.customer AS customer',DB::raw('sum(consignments.totalquantity) as grosslitres'),DB::raw('sum(deportdatas.litre) as litre'),DB::raw('sum(deportdatas.vcf) as metricTonAir'),DB::raw('sum(deportdatas.vac) as vac'))
//                ->groupBy('deportdatas.customer','consignments.totalquantity', 'consignments.reference_number', 'deportdatas.litre', 'deportdatas.vcf', 'deportdatas.vac')->where('deportdatas.consignmentname',trim($request->consignmentname))
//                ->where('deportdatas.status','valid')
//                ->whereDate('deportdatas.created_at','>=',$fromDate)
//                ->whereDate('deportdatas.created_at','<=',$toDate)->get();

            $html=view('admin.pdfs.dailyrecord')->with([
                'dailyrecord'=>$dailyrecord,
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                'fieldrep'=>$fieldrep,
                'contact'=>$repContact->tel,
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,
                'summarysheet'=>$summarysheet,


            ])->render();
        }
        elseif ($typereport == "2"){ //SUMMARY REPORT
            
            
            $pdfname='SUMMARY_REPORT-'.$depotname.'_'.$product->product.'_'.date('Y-M-d');
            
            $mainconsignment= DB::table('mainconsignments')->where('consignmentname',trim($request->consignmentname))->first();
            
            $records= DB::table('deportdatas')
                ->join('consignments', 'deportdatas.consignmentname', '=', 'consignments.consignmentname')
                ->select(DB::raw('date(deportdatas.created_at) as date'),DB::raw('SUM(deportdatas.tonmetricair) as quantityloaded'))
                ->groupBy('date')
                ->where('deportdatas.consignmentname',trim($request->consignmentname))
                ->where('deportdatas.status','valid')
                ->whereDate('deportdatas.created_at','>=',$fromDate)
                ->whereDate('deportdatas.created_at','<=',$toDate)
                ->get();
            
            $initialConsignmentInfo=DB::table('consignments')
                  ->select('totalquantity',DB::raw('date(created_at) as date'))
                  ->where('consignmentname',trim($request->consignmentname))
                  ->first();

            $html=view('admin.pdfs.dailysummary')->with([
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                'fieldrep'=>$fieldrep,
                'contact'=>is_null($repContact) ? "": $repContact->tel ,
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,
                'initialConsignmentInfo'=>$initialConsignmentInfo,
                'records'=>$records,
                'mainconsignment'=>$mainconsignment->quantity,
                'totalquantity_in_Gross_litres'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity)
                ])->render();
            
            
            
            
            
        }
        elseif ($typereport == "3"){
            $pdfname='CUMULATIVE-'.$depotname.'_'.$product->product.'_'.date('Y-M-d');


            $initialConsignmentInfo=DB::table('consignments')
                ->select('totalquantity',DB::raw('date(created_at) as date'))
                ->where('consignmentname',trim($request->consignmentname))
                ->first();

            $dailyrecord =  DB::table('deportdatas')
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                ->where('supervisor',$supervisor)
                ->where('status','valid')->get();
 //dd($dailyrecord);
            $dailyrecord2 =  DB::table('deportdatas')
                ->select([DB::raw('COUNT(*) as number,customer,SUM(obslitre) as obslitre,SUM(litre) as litre,SUM(vac) as vac,SUM(tonmetricair) as tonmetricair')])
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                ->where('supervisor',$supervisor)->where('status','valid')
                ->groupBy('customer')
                ->orderBy('obslitre','asc')
                ->get();
            //dd($dailyrecord2);
            $allDate=  DB::table('deportdatas')->select([DB::raw('distinct date(created_at) as dates')])
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                ->where('supervisor',$supervisor)
                ->where('status','valid')
                ->get();
  //dd($allDate);
            $html=view('admin.pdfs.cummulativestock')->with([
                'allDate'=>$allDate,
                'dailyrecord'=>$dailyrecord,
                'dailyrecord2'=>$dailyrecord2,
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                'supervisor'=>$supervisor,
                'contact'=>is_null($supContact) ? "": $supContact->tel ,
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,
                'initialConsignmentInfo'=>$initialConsignmentInfo,
            ])->render();
        }

        $this->downloadPdf($pdfname,$html);
    }

    //for the Bank
    public function dailyreportbank(Request $request){
        
        $pdfname=null;
        $fromDate=$request->startdate;
        $toDate=$request->enddate;
        $typereport=$request->typereport;
        $bdc=$request->bdc;
        $depotname=$request->depotname;
       //$fieldrep=trim($request->fieldrep);
        //$supervisor=trim($request->supervisor);


        $depotlocation= DB::table('depots')->select([DB::raw('location')])->where('name',$depotname)->first();
        $bank=$request->bank;
        //dd($bank);
        $product=DB::table('consignments')->select([DB::raw('product')])->where('consignmentname',$request->consignmentname)->first();
        $grossMetricTonInAir=DB::table('consignments')->select([DB::raw('totalquantity')])->where('consignmentname',$request->consignmentname)->first();


        //$repContact=DB::table('users')->select([DB::raw('tel')])->where('name',$fieldrep)->first();
       // $supContact=DB::table('users')->select([DB::raw('tel')])->where('name',$supervisor)->first();

        if($typereport == "1"){//DAILY REPORT
            $pdfname='Daily_'.$depotname.'_'.$product->product.'_'.date('Y-M-d');
            $dailyrecord =  DB::table('deportdatas')->whereDate('created_at','>=',$fromDate)
                ->whereDate('created_at','<=',$toDate)->where('depotname',$depotname)->where('bank',$bank)->where('bdc',$bdc)
               // ->where('user',$fieldrep)
                ->where('status','valid')->get();
            $summarysheet= DB::table('deportdatas')
                ->select([DB::raw('COUNT(*) as number,customer,SUM(obslitre) as obslitre,SUM(litre) as litre,SUM(vac) as vac,SUM(tonmetricair) as tonmetricair')])
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                //->where('supervisor',$supervisor)
                ->where('status','valid')
                ->groupBy('customer')
                ->orderBy('obslitre','asc')
                ->get();


//
//                DB::table('deportdatas')
//                ->join('consignments', 'deportdatas.consignmentname', '=', 'consignments.consignmentname')
//                ->select(DB::raw('count(deportdatas.customer) as number'),'deportdatas.customer AS customer',DB::raw('sum(consignments.totalquantity) as grosslitres'),DB::raw('sum(deportdatas.litre) as litre'),DB::raw('sum(deportdatas.vcf) as metricTonAir'),DB::raw('sum(deportdatas.vac) as vac'))
//                ->groupBy('deportdatas.customer','consignments.totalquantity', 'consignments.reference_number', 'deportdatas.litre', 'deportdatas.vcf', 'deportdatas.vac')->where('deportdatas.consignmentname',trim($request->consignmentname))
//                ->where('deportdatas.status','valid')
//                ->whereDate('deportdatas.created_at','>=',$fromDate)
//                ->whereDate('deportdatas.created_at','<=',$toDate)->get();

            $html=view('bank.pdfs.dailyrecord')->with([
                'dailyrecord'=>$dailyrecord,
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                //'fieldrep'=>$fieldrep,
                //'contact'=>$repContact->tel,
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,
                'summarysheet'=>$summarysheet,


            ])->render();
        }
        elseif ($typereport == "2"){ //SUMMARY REPORT


            $pdfname='SUMMARY_REPORT-'.$depotname.'_'.$product->product.'_'.date('Y-M-d');

            $mainconsignment= DB::table('mainconsignments')->where('consignmentname',trim($request->consignmentname))->first();

            $records= DB::table('deportdatas')
                ->join('consignments', 'deportdatas.consignmentname', '=', 'consignments.consignmentname')
                ->select(DB::raw('date(deportdatas.created_at) as date'),DB::raw('SUM(deportdatas.tonmetricair) as quantityloaded'))
                ->groupBy('date')
                ->where('deportdatas.consignmentname',trim($request->consignmentname))
                ->where('deportdatas.status','valid')
                ->whereDate('deportdatas.created_at','>=',$fromDate)
                ->whereDate('deportdatas.created_at','<=',$toDate)
                ->get();

            $initialConsignmentInfo=DB::table('consignments')
                ->select('totalquantity',DB::raw('date(created_at) as date'))
                ->where('consignmentname',trim($request->consignmentname))
                ->first();

            $html=view('bank.pdfs.dailysummary')->with([
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                //'fieldrep'=>$fieldrep,
               // 'contact'=>is_null($repContact) ? "": $repContact->tel ,
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,
                'initialConsignmentInfo'=>$initialConsignmentInfo,
                'records'=>$records,
                'mainconsignment'=>$mainconsignment->quantity,
                'totalquantity_in_Gross_litres'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity)
            ])->render();





        }
        elseif ($typereport == "3"){
            //dd("Ok");
            $pdfname='CUMULATIVE-'.$depotname.'_'.$product->product.'_'.date('Y-M-d');


            $initialConsignmentInfo=DB::table('consignments')
                ->select('totalquantity',DB::raw('date(created_at) as date'))
                ->where('consignmentname',trim($request->consignmentname))
                ->first();

            $dailyrecord =  DB::table('deportdatas')
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                //->where('supervisor',$supervisor)
                ->where('status','valid')->get();

            $dailyrecord2 =  DB::table('deportdatas')
                ->select([DB::raw('COUNT(*) as number,customer,SUM(obslitre) as obslitre,SUM(litre) as litre,SUM(vac) as vac,SUM(tonmetricair) as tonmetricair')])
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                //->where('supervisor',$supervisor)
                ->where('status','valid')
                ->groupBy('customer')
                ->orderBy('obslitre','asc')
                ->get();
            //dd($dailyrecord2);
            $allDate=  DB::table('deportdatas')->select([DB::raw('distinct date(created_at) as dates')])
                ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                //->where('supervisor',$supervisor)
                ->where('status','valid')
                ->get();

            $html=view('bank.pdfs.cummulativestock')->with([
                'allDate'=>$allDate,
                'dailyrecord'=>$dailyrecord,
                'dailyrecord2'=>$dailyrecord2,
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                //'supervisor'=>$supervisor,
                //'contact'=>is_null($supContact) ? "": $supContact->tel ,
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,
                'initialConsignmentInfo'=>$initialConsignmentInfo,
            ])->render();
        }

        $this->downloadPdf($pdfname,$html);
    }

    //for the Bdc
    public function dailyreportbdc(Request $request){
        $pdfname=null;
        $fromDate=$request->startdate;
        $toDate=$request->enddate;
        $typereport=$request->typereport;
        $bdc=$request->bdc;
        $depotname=$request->depotname;
        //$fieldrep=trim($request->fieldrep);

        $depotlocation= DB::table('depots')->select([DB::raw('location')])->where('name',$depotname)->first();
        $bank=$request->bank;
        $product=DB::table('consignments')->select([DB::raw('product')])->where('consignmentname',$request->consignmentname)->first();
        $grossMetricTonInAir=DB::table('consignments')->select([DB::raw('totalquantity')])->where('consignmentname',$request->consignmentname)->first();


       // $repContact=DB::table('users')->select([DB::raw('tel')])->where('name',$fieldrep)->first();
        if($typereport == "1"){
            $pdfname='Daily_'.$depotname.'_'.$product->product.'_'.date('Y-M-d');
            $dailyrecord =  DB::table('deportdatas')
                ->whereDate('created_at','>=',$fromDate)
                ->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
                //->where('user',$fieldrep)
                ->where('status','valid')

                ->get();

            $html=view('admin.pdfs.dailyrecord')->with([
                'dailyrecord'=>$dailyrecord,
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                'fieldrep'=>'',
                'contact'=>'',
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,


            ])->render();
        }
        elseif ($typereport == "2"){
            $pdfname='Daily_'.$depotname.'_'.$product->product.'_'.date('Y-M-d');
            $records= DB::table('deportdatas')
                ->join('consignments', 'deportdatas.consignmentname', '=', 'consignments.consignmentname')
                ->select(DB::raw('count(deportdatas.customer) as number'),'deportdatas.customer AS customer',DB::raw('sum(consignments.totalquantity) as grosslitres'),DB::raw('sum(deportdatas.litre) as litre'),DB::raw('sum(deportdatas.vcf) as metricTonAir'),DB::raw('sum(deportdatas.vac) as vac'))
                ->groupBy('deportdatas.customer','consignments.totalquantity', 'consignments.reference_number', 'deportdatas.litre', 'deportdatas.vcf', 'deportdatas.vac')
                ->where('deportdatas.consignmentname',trim($request->consignmentname))
                ->where('deportdatas.status','valid')
                ->whereDate('deportdatas.created_at','>=',$fromDate)
                ->whereDate('deportdatas.created_at','<=',$toDate)
                ->get();
            $html=view('admin.pdfs.dailysummary')->with([
                'records'=>$records,
            ])->render();
        }
        elseif ($typereport == "3"){
            $pdfname='Daily_'.$depotname.'_'.$product->product.'_'.date('Y-M-d');
            $dailyrecord =  DB::table('deportdatas')
                ->whereDate('created_at','>=',$fromDate)
                ->whereDate('created_at','<=',$toDate)
                ->where('depotname',$depotname)
                ->where('bank',$bank)
                ->where('bdc',$bdc)
               // ->where('user',$fieldrep)
                ->where('status','valid')
                ->get();
            $html=view('admin.pdfs.cummulativestock')->with([
                'dailyrecord'=>$dailyrecord,
                'depotname'=>$depotname,
                'depotlocation'=>$depotlocation->location,
                'bdc'=>$bdc,
                'bank'=>$bank,
                'product'=>$product->product,
                'fieldrep'=>'',
                'contact'=>'',
                'gross_litres_in_tank'=>$this->getGrosslitres_in_air($product->product,$grossMetricTonInAir->totalquantity),
                'gross_litres_in_air'=>$grossMetricTonInAir->totalquantity,
            ])->render();
        }
        $pdf= new  PDF();
        $pdf::filename($pdfname.'.pdf');
        return $pdf::load($html, 'A4', 'landscape')->download();

    }




    private function getGrosslitres_in_air($product,$grossInAir){
    if($product=='PMS'){
        $grossintank=((integer)$grossInAir*1.134)*1000;
    }elseif ($product=='AGO'){
        $grossintank=((integer)$grossInAir*1.182)*1000;
    }elseif ($product=='RFO'){
        $grossintank=((integer)$grossInAir*1.182)*1000;
    }elseif ($product=='ATK'){
        $grossintank=((integer)$grossInAir*1.182)*1000;
    }else{
        $grossintank=((integer)$grossInAir*0)*1000;
    }
    return $grossintank;
}
    /////////////////////BANK//////////////////////////
    public function downloadpdfreportbank($reference_number,$bank)
    {
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select('consignments.*', DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.bank',$bank)
            ->where('deportdatas.status','valid')
            ->where('consignments.reference_number',$reference_number)->get()->first();

        $html=view('bank.pdfs.consignmentbank')->with([
            'consignment'=>$consignment,
            


        ])->render();

         $mypdf= new  PDF();
         $mypdf::filename('bankpdf');
        return $mypdf::load($html, 'A4', 'landscape')->download();

    }

    /////////////////////BDC//////////////////////////
    public function downloadpdfreportbdc($reference_number,$bdc)
    {
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select('consignments.*', DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.bdc',$bdc)
            ->where('deportdatas.status','valid')
            ->where('consignments.reference_number',$reference_number)->get()->first();

        $html=view('bdc.pdfs.consignmentbdc')->with([
            'consignment'=>$consignment,



        ])->render();
        $mypdf= new  PDF();
        $mypdf::filename('bdcpdf');
        return $mypdf::load($html, 'A4', 'landscape')->download();

    }


    private function downloadPdf($pdfname,$html){
        $pdf= new  PDF();
        $pdf::filename($pdfname.'.pdf');
        return $pdf::load($html, 'A4', 'landscape')->download();
    }
}
