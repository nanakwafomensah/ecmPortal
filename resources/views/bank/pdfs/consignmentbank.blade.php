
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>consignment</title>
    {{--<link rel="stylesheet" href="pdfstyle/style.css" media="all" />--}}
    {{--<link rel="stylesheet" href="pdfstyle/style.css" media="all" />--}}

    <style>
        #datarecord {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        #datarecord td, #datarecord th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: silver;
            color: white;
            font-size: 12px;
        }

    </style>
</head>
<body >
<div class="container">
   <h3><center><b>CONSIGNMENT DETAILS</b></center></h3>

        <img style="float: right; " src="img/ecm_logo.jpg" width="110px" height="40px" alt="logo" />
    <table id="datarecord" style="width: 100%;">
        <thead>
        <tr>
            <th>Title </th>
            <th></th>
        </tr>
        </thead>
        <tbody>


            <tr>
                <td>REFERENCE NUMBER:</td>
                <td>{{$consignment->reference_number}}</td>

            </tr>
            <tr>
                <td>PRODUCT:</td>
                <td>{{$consignment->product}}</td>

            </tr>
            <tr>
                <td>DEPOT:</td>
                <td>{{$consignment->depot}}</td>

            </tr>
            <tr>
                <td>TOTAL QUANTITY:</td>
                <td>{{$consignment->totalquantity}}</td>

            </tr>
            <tr>
                <td>TOTAL RECORDED:</td>
                <td>{{$consignment->totalrecorded}}</td>

            </tr>

        <tbody>

    </table>


</div>
</body>
</html>