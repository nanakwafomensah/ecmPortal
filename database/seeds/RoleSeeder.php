<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->delete();
        DB::table('roles')->insert(array(
            array('slug'=>'Admin','name'=>'Admin'),
            array('slug'=>'Fieldrep','name'=>'Fieldrep'),
            array('slug'=>'Supervisor','name'=>'Supervisor'),
            array('slug'=>'Bdcrep','name'=>'Bdcrep'),
            array('slug'=>'Bankrep','name'=>'Bankrep'),

        ));
    }
}
