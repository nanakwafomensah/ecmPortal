<?php

namespace App\Http\Controllers;

use App\Depot;
use DB;
use Illuminate\Http\Request;

class DeportController extends Controller
{
    //
    public function index(){

        $depots=DB::table('depots')->orderBy('created_at', 'desc')->get();
        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();

        return view("admin/entry/deport")->with([
            'depots'=>$depots,
            'constants'=>$constants,
            'consignment'=>$consignment
        ]);
    }
    public function save( Request $request){

        try{
            $m = new Depot();

            $m->name=$request->name;
            $m->location=$request->address;
            $m->phonenumber=$request->phonenumber;
            

            if($m->save()){
                $notification=array(
                    'message'=>"New Depot  has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('deport')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('deport')->with($notification);
        }
    }

    public function update(Request $request){

        try{
            $m = Depot::findorfail($request->idedit);
            $m->name=$request->nameedit;
            $m->location=$request->addressedit;
            $m->phonenumber=$request->phonenumberedit;


            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('deport')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('deport')->with($notification);
        }
    }
    public function delete(Request $request){
        try{
            $m = Depot::findorfail($request->iddelete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Depot has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('deport')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('deport')->with($notification);
        }
    }
}
