<?php

namespace App\Http\Controllers;

use App\Consignment;
use Illuminate\Http\Request;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use DB;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class LoginController extends Controller
{
    //
    public  function index(){
        return view('admin.login');
    }
    public function postLogin(Request $request){
        try{
            $credentials = [
                'email'    => $request->email,
                'password' => $request->password,
            ];

            
            if(Sentinel::authenticate($credentials)){
                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                {
                    return redirect('dashboard');

                }elseif(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Fieldrep')
                {
                    return redirect('dailyrecord');

                }elseif(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Bdcrep')
                {
                    return redirect('bdcindex');

                }elseif(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Bankrep')
                {
                   
                    return redirect('bankindex');

                }
                else{
                    return redirect('/');
                }
            }else
            {

                return redirect()->back()->with(['error'=>'wrong credentials']);

            }
        }catch(ThrottlingException $e){
            $delay=$e->getDelay();
            return redirect()->back()->with(['error'=>"You are banned for $delay seconds."]);

        }catch (NotActivatedException $e){
            return redirect()->back()->with(['error'=>"Your account has not been activated"]);
        }

    }
    public function postlogout(){
        Sentinel::logout();
        return redirect('/');
    }
}
