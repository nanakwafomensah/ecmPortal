
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">

        <input id="tresholdhidden"  type="hidden" value="<?php if(isset($constants->totalquantity)){echo 0.1 * $constants->totalquantity; }?>"/>
        <input id="currenttresholdhidden" type="hidden" value="<?php if(isset($consignment->totalrecorded)){echo $consignment->totalrecorded;}?>"/>
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a>
            <a class="brand" href="dashboard">EXPERT COLLATERAL & MONITORING PORTAL   &nbsp;<h5><i class="icon-user"></i> Hello,{{Sentinel::getUser()->first_name}}</h5></a>
            <div class="nav-collapse">
                <ul class="nav pull-right">


                    @if(Sentinel::check())

                        <form action="{{route('logout')}}" method="post" id="logout-form">
                            {{csrf_field()}}
                            <a class="btn" style="color: darkred;background-color: white;" href="#" onclick="document.getElementById('logout-form').submit()" style="color: black"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>

                        </form>


                    @endif

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /container -->
    </div>
    <!-- /navbar-inner -->
</div>
<!-- /navbar -->
<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">
                <li ><a href="dashboard"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
                <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Information Management</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="bank">BANK</a></li>
                        <li><a href="deport">DEPOT</a></li>
                        <li><a href="bdc">BDC</a></li>
                        <li><a href="mainconsignment">BANK CONSIGNMENT</span> </a> </li>
                        <li><a href="consignment">CONSIGNMENT RELEASES</span> </a> </li>
                        <li><a href="constant">CONSTANT</span> </a> </li>
                        <li><a href="user">USERS</span> </a> </li>
                        <li><a href="reprecord">FIELD REP RECORDS</span> </a> </li>



                    </ul>
                </li>

                <li><a href="report"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>

            </ul>
        </div>
        <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
</div>
<!-- /subnavbar -->