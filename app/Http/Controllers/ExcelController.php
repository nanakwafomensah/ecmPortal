<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
class ExcelController extends Controller
{
    //
    public function downloadExcelreportbank($type,$reference_number,$bank){
        $data = DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select('consignments.reference_number','consignments.product','consignments.depot','consignments.totalquantity', DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.bank',$bank)
            ->where('consignments.reference_number',$reference_number)->get()->toArray();
        $data=json_decode(json_encode($data),true);
        return Excel::create('data', function($excel) use ($data) {
            $excel->sheet('csv', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function downloadExcelreportbdc($type,$reference_number,$bdc){
        $data = DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select('consignments.reference_number','consignments.product','consignments.depot','consignments.totalquantity', DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname',
                'consignments.id',
                'consignments.reference_number',
                'consignments.supplier',
                'consignments.date_issued',
                'consignments.date_expire',
                'consignments.totalquantity',
                'consignments.description',
                'consignments.port_of_discharge',
                'consignments.product',
                'consignments.bank',
                'consignments.depot',
                'consignments.bdc',
                'consignments.contact',
                'consignments.status',
                'consignments.created_at',
                'consignments.updated_at')
            ->where('consignments.bdc',$bdc)
            ->where('consignments.reference_number',$reference_number)->get()->toArray();
        $data=json_decode(json_encode($data),true);
        return Excel::create('data', function($excel) use ($data) {
            $excel->sheet('csv', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
}
