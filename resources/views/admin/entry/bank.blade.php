<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
@include('layout.admin.navbarToplayout')
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header" style="background: white !important; ">

                            <h3 style="color:darkred !important;">Bank</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->
                    <button class="btn btn-lg" style="background-color:darkred;color: #fff" data-toggle="modal" data-target="#addbankModal">Add New</button><span>&nbsp;</span>


                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Bank Name</th>
                            <th>Location</th>

                            <th>PhoneNumber</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Depot Name</th>
                            <th>Location</th>

                            <th>PhoneNumber</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <?php
                        $i=1;
                        ?>
                        @foreach($banks as $bank)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$bank->name}}</td>
                                <td>{{$bank->address}}</td>
                                <td>{{$bank->phonenumber}}</td>
                                <td>{{$bank->created_at}}</td>
                                <td>{{$bank->updated_at}}</td>
                                <td>
                                    <div class="controls">
                                        <div class="btn-group">
                                            <a class="btn " style="background-color:white;color: darkred" href="#"><i class="icon-action icon-white"></i>Select</a>
                                            <a class="btn  dropdown-toggle" style="background-color:darkred;color: #fff" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-toggle="modal"  data-target="#editbankModal" class="editbtn" data-id="{{$bank->id}}" data-name="{{$bank->name}}" data-address="{{$bank->address}}" data-phonenumber="{{$bank->phonenumber}}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a data-toggle="modal"  data-target="#deletebankModal" class="deletebtn" data-id="{{$bank->id}}" data-name="{{$bank->name}}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>	<!-- /controls -->
                                </td>

                            </tr>
                            @endforeach
                            </tbody>
                    </table>


                </div> <!-- /widget -->

            </div> <!-- /span8 -->



            <!-- Modal -->
            <div id="addbankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">New Bank</h4>
                        </div>
                        <form action="savebank" method="post">
                            <div class="modal-body">

                                <div class="control-group">
                                    <label class="control-label" for="firstname">Bank Name</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="name" name="name"  required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Address</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="address" name="address" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Phone Number</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="phonenumber" name="phonenumber" required>
                                    </div> <!-- /controls -->
                                </div>


                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="editbankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Bank</h4>
                        </div>
                        <form action="updatebank" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="bank_edit_id" name="idedit" />
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Company Name</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="name_edit" name="nameedit"  required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Address</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="address_edit" name="addressedit" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Phone Number</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="phonenumber_edit" name="phonenumberedit" required>
                                    </div> <!-- /controls -->
                                </div>


                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save Changes</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="deletebankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete BDC</h4>
                        </div>
                        <form action="deletebank" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="bank_delete_id" name="iddelete" />

                                <p>Are you sure you want to delete <span id="nameOfbank"></span></p>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Delete</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main -->








<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="asset/js/toastr.min.js"></script>
<script src="asset/js/utility.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('click','.editbtn',function() {

        $('#bank_edit_id').val($(this).data('id'));
        $('#name_edit').val($(this).data('name'));
        $('#address_edit').val($(this).data('address'));
        $('#phonenumber_edit').val($(this).data('phonenumber'));


    });
    $(document).on('click','.deletebtn',function() {
        $('#bank_delete_id').val($(this).data('id'));
        $('#nameOfbank').html($(this).data('name'));
    });
</script>
</body>

</html>
