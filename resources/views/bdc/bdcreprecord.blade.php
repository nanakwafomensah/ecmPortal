<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">
    <link href="asset/css/style.css" rel="stylesheet">
    <link href="asset/css/pages/dashboard.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>


<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html">ECM PORTAL    &nbsp;<h5><i class="icon-user"></i> Hello,{{Sentinel::getUser()->first_name}}</h5></a>
            <div class="nav-collapse">
                <ul class="nav pull-right">


                    @if(Sentinel::check())

                        <form action="{{route('logout')}}" method="post" id="logout-form">
                            {{csrf_field()}}
                            <a class="btn" style="color: darkred;background-color: white;" href="#" onclick="document.getElementById('logout-form').submit()" style="color: black"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>

                        </form>


                    @endif

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /container -->
    </div>
    <!-- /navbar-inner -->
</div>
<!-- /navbar -->
<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">

                <li class=""><a href="bdcindex"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
                <li class=""><a href="reportbdc"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
                <li class="active"><a href="bdcreprecord"><i class="icon-list-alt"></i><span>Field Records</span> </a> </li>
            </ul>
        </div>
        <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
</div>
<!-- /subnavbar -->



<div class="main">

    <div class="main-inner">

        <div class="container">
            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header" style="background: white !important; ">

                            <h3 style="color:darkred !important;">Field Rep Records</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->

                    {{--<div><center> <span>TempDeg:<strong> {{$constant->temDeg}}</strong></span>&nbsp; &nbsp;<span>Density: <strong>{{$constant->density}}</strong></span>&nbsp; &nbsp; <span>VCF: <strong>{{$constant->vcf}}</strong></span> </center></div>--}}
                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Recorded Date</th>
                            <th>Consignment Name</th>

                            <th>VehicleNumber</th>
                            <th>driver name</th>

                            <th>shore tank</th>
                            <th>Order Number</th>
                            <th>Way Bill Number</th>
                            <th>Customer</th>
                            <th>Obs Litre</th>
                            <th>Litre</th>
                            <th>VAC</th>
                            <th>Ton metric in air</th>


                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Recorded Date</th>
                            <th>Consignment Name</th>
                            <th>VehicleNumber</th>
                            <th>driver name</th>
                            <th>shore tank</th>
                            <th>Order Number</th>
                            <th>Way Bill Number</th>
                            <th>Customer</th>
                            <th>Obs Litre</th>
                            <th>Litre</th>
                            <th>VAC</th>
                            <th>Ton metric in air</th>

                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $i=1;
                        ?>
                        @foreach($dailyrecord as $d )
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$d->created_at}}</td>

                                <td>{{$d->consignmentname}}</td>
                                <td>{{$d->vehiclenumber}}</td>
                                <td>{{$d->drivername}}</td>
                                <td>{{$d->shoretank}}</td>
                                <th>{{$d->orderno}}</th>
                                <td>{{$d->waybillno}}</td>
                                <td>{{$d->customer}}</td>
                                <td>{{$d->obslitre}}</td>
                                <td>{{$d->litre}}</td>
                                <td>{{ sprintf('%0.3f',$d->vac)}}</td>
                                <td>{{sprintf('%0.3f',$d->tonmetricair)}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- /widget -->

            </div> <!-- /span8 -->


        </div> <!-- /container -->

    </div> <!-- /main-inner -->


</div>
<!-- /main -->


<!-- /footer -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="asset/js/jquery-1.7.2.min.js"></script>
<script src="asset/js/bootstrap.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</body>
</html>
