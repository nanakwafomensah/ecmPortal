<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
@include('layout.admin.navbarToplayout')

<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header" style="background: white !important; ">

                            <h3 style="color:darkred !important;">Field Rep Records</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->

                    {{--<div><center> <span>TempDeg:<strong> {{$constant->temDeg}}</strong></span>&nbsp; &nbsp;<span>Density: <strong>{{$constant->density}}</strong></span>&nbsp; &nbsp; <span>VCF: <strong>{{$constant->vcf}}</strong></span> </center></div>--}}
                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Consignment Name</th>

                            <th>VehicleNumber</th>
                            <th>driver name</th>

                            <th>shore tank</th>
                            <th>Order Number</th>
                            <th>Way Bill Number</th>
                            <th>Customer</th>
                            <th>Obs Litre</th>
                            <th>Litre</th>
                            <th>VAC</th>
                            <th>Ton metric in air</th>
                            <th>Action </th>

                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Consignment Name</th>
                            <th>VehicleNumber</th>
                            <th>driver name</th>
                            <th>shore tank</th>
                            <th>Order Number</th>
                            <th>Way Bill Number</th>
                            <th>Customer</th>
                            <th>Obs Litre</th>
                            <th>Litre</th>
                            <th>VAC</th>
                            <th>Ton metric in air</th>
                            <th>Action </th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $i=1;
                        ?>
                        @foreach($dailyrecord as $d )
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$d->consignmentname}}</td>
                                <td>{{$d->vehiclenumber}}</td>
                                <td>{{$d->drivername}}</td>
                                <td>{{$d->shoretank}}</td>
                                <th>{{$d->orderno}}</th>
                                <td>{{$d->waybillno}}</td>
                                <td>{{$d->customer}}</td>
                                <td>{{$d->obslitre}}</td>
                                <td>{{$d->litre}}</td>
                                <td>{{ sprintf('%0.3f',$d->vac)}}</td>
                                <td>{{sprintf('%0.3f',$d->tonmetricair)}}</td>
                                <td>
                                    <div class="controls">
                                        <div class="btn-group">
                                            <a class="btn " style="background-color:white;color: darkred" href="#"><i class="icon-action icon-white"></i>Select</a>
                                            <a class="btn  dropdown-toggle" style="background-color:darkred;color: #fff" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-toggle="modal"  data-target="#editBdcModal" class="editbtn"  data-id="{{$d->id}}" data-vehiclenumber="{{$d->vehiclenumber}}" data-drivername="{{$d->drivername}}" data-shoretank="{{$d->shoretank}}"
                                                       data-orderno="{{$d->orderno}}"
                                                       data-waybillno="{{$d->waybillno}}"
                                                       data-customer="{{$d->customer}}"
                                                       data-obslitre="{{$d->obslitre}}"
                                                       data-consignmentname="{{$d->consignmentname}}"

                                                       data-tempdeg="{{$d->tempdeg}}"
                                                       data-density="{{$d->density}}"
                                                       data-vcf="{{$d->vcf}}"
                                                    ><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a class="deletebtn"  data-toggle="modal"  data-target="#deleteBdcModal" data-id="{{$d->id}}" ><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>	<!-- /controls -->
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- /widget -->

            </div> <!-- /span8 -->





            <div id="editBdcModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Daily Report</h4>
                        </div>
                        <form action="updatedailyapprove" method="post">
                            <input type="hidden" name="temdegedit" id="temdegedit" />
                            <input type="hidden" name="densityedit" id="densityedit" />
                            <input type="hidden" name="vcfedit" id="vcfedit" />
                            <div class="modal-body">
                                <input type="hidden" id="idedit" name="idedit" />
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Consignment NAme</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="consignmentnameedit" name="consignmentnameedit"  required disabled>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Vehicle Number</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="vehiclenumberedit" name="vehiclenumberedit"  required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Driver Name</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="drivernameedit" name="drivernameedit" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Shore Tank</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="shoretankedit" name="shoretankedit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">Order  Number</label>

                                    <div class="controls">
                                        <input type="text" class="span5" id="ordernumberedit" name="ordernumberedit" required>
                                    </div>
                                </div> <!-- /control-group -->
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">Way Bill Number</label>

                                    <div class="controls">
                                        <input type="text" class="span5" id="waybillnumberedit" name="waybillnumberedit" required>
                                    </div>
                                </div> <!-- /control-group -->
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">customer</label>

                                    <div class="controls">

                                        <select id="customeredit" name="customeredit" required>
                                            <option>AGAPET LIMITED</option>
                                            <option>AI ENERGY GROUP LIMITED</option>
                                            <option>ALINCO OIL COMPANY LIMITED</option>
                                            <option>ALIVE GAS</option>
                                            <option>ALLIED OIL COMPANY LIMITED</option>
                                            <option>ANASSET COMPANY LIMITED</option>
                                            <option>ANDEV COMPANY LIMITED</option>
                                            <option>ANNANDALE GHANA LIMITED</option>
                                            <option>AP OIL & GAS GHANA LIMITED</option>
                                            <option>APEX PETROLEUM GHANA LIMITED</option>
                                            <option>ASPEN PETROLEUM</option>
                                            <option>AVOS OIL & GAS</option>
                                            <option>BAFFOUR GAS COMPANY LIMITED</option>
                                            <option>BANO OIL COMPANY LIMITED</option>
                                            <option>BEAP ENERGY GHANA LIMITED</option>
                                            <option>BENAB OIL COMPANY LIMITED</option>
                                            <option>BF PETROLEUM LIMITED</option>
                                            <option>BG PETROLEUM LIMITED</option>
                                            <option>BISVEL PETROLEUM SERVICES</option>
                                            <option>BLANKO OIL COMPANY LIMITED</option>
                                            <option>CAPSTONE OIL LIMITED</option>
                                            <option>CASH OIL COMPANY LIMITED</option>
                                            <option>CENT EASTERN GAS LIMITED</option>
                                            <option>CENTRAL BRENT PETROLEUM LIMITED</option>
                                            <option>CHAMPION OIL CO. LTD</option>
                                            <option>COEGAN GHANA LIMITED</option>
                                            <option>COMPASS OLEUM LIMITED</option>
                                            <option>CORNOIL PETROLEUM LIMITED</option>
                                            <option>CROWN PETROLEUM GH. LTD</option>
                                            <option>DA OIL CO. LTD</option>
                                            <option>DABEMENS GAS CO.</option>
                                            <option>DELIMAN  & COMPANY LTD</option>
                                            <option>DUKES PETROLEUM COMPANY LTD.</option>
                                            <option>ENGEN GHANA LTD</option>
                                            <option>EV. OIL CO. LTD</option>
                                            <option>EXCEL OIL CO. LTD</option>
                                            <option>FIRST GAS COMPANY LIMITED</option>
                                            <option>FINEST OIL COMPANY LIMITED</option>
                                            <option>FRAGA OIL GH. LTD</option>
                                            <option>FRIMPS OIL CO. LTD</option>
                                            <option>FRONTIER OIL GHANA LIMITED</option>
                                            <option>GALAXY OIL CO. LTD</option>
                                            <option>GASO PETROLEUM LIMITED</option>
                                            <option>GHANA OIL COMPANY LIMITED</option>
                                            <option>GLASARK OIL CO. LTD</option>
                                            <option>GLEE OIL LIMITED</option>
                                            <option>GLOBAL STANDARD PETROLEUM LIMITED</option>
                                            <option>GLORY OIL CO. LTD</option>
                                            <option>GO-GAS VENTURES LIMITED</option>
                                            <option>GOODNESS ENERGY LIMITED</option>
                                            <option>G&G OIL COMPANY LIMITED</option>
                                            <option>GRACE OIL PETROLEUM CO. LTD</option>
                                            <option>GRID PETROLEUM GHANA LIMITED</option>
                                            <option>GULF ENERGY GHANA LIMITED</option>
                                            <option>HAK OIL CO. LTD</option>
                                            <option>HAVILAH OIL GHANA LTD</option>
                                            <option>HILLS OIL MARKETING COMPANY LIMITED</option>
                                            <option>HOLMAN PETROLEUM</option>
                                            <option>HOSSANA OIL COMPANY LIMITED</option>
                                            <option>INFIN GHANA LIMITED</option>
                                            <option>JO & JU OIL COMPANY LTD</option>
                                            <option>JOEKONA COMPANY LIMITED</option>
                                            <option>JUSBRO PETROLEUM CO. LTD</option>
                                            <option>KABORE OIL LIMITED</option>
                                            <option>KAN ROYAL SERVICE STATION & TRADING LIMITED</option>
                                            <option>KARELA OIL AND GAS LIMITED</option>
                                            <option>KAYSENS GAS COMPANY</option>
                                            <option>KINGS ENERGY LIMITED</option>
                                            <option>KI ENERGY LIMITED</option>
                                            <option>LAMBARK GAS COMPANY LIMITED</option>
                                            <option>LAMININ BEE VENTURES LIMITED</option>
                                            <option>LIFE PETROLEUM COMPANY LTD</option>
                                            <option>LILYGOLD ENERGY RESOURCES LIMITED</option>
                                            <option>LONESTAR GAS COMPANY LIMITED</option>
                                            <option>LOUIS GAS COMPANY LIMITED</option>
                                            <option>LUCKY OIL CO. LTD</option>
                                            <option>MANBAH GAS COMPANY LIMITED</option>
                                            <option>MAXX GAS LIMITED</option>
                                            <option>MAXX ENERGY LIMITED</option>
                                            <option>MAXXON PETROLEUM LIMITED</option>
                                            <option>MERCY OIL MARKETING COMPANY LIMITED</option>
                                            <option>MIDAS OIL & GAS LIMITED</option>
                                            <option>MIGHTY GAS COMPANY LIMITED</option>
                                            <option>MODEX OIL CO. LTD</option>
                                            <option>MOTOHAUS OIL CO. LTD</option>
                                            <option>MS OIL</option>
                                            <option>NAAGAMNI GHANA LTD</option>
                                            <option>NASONA OIL COMPANY LIMITED</option>
                                            <option>NEXTBONS GAS LIMITED</option>
                                            <option>NICK PETROLEUM GHANA LIMITED</option>
                                            <option>NORGAZ PETROLEUM LTD</option>
                                            <option>O.J.K COMPANY LIMITED</option>
                                            <option>OANDO GHANA LIMITED</option>
                                            <option>OCEAN OIL COMPANY LIMITED</option>
                                            <option>OMEGA ENERGY LTD</option>
                                            <option>ORIENT ENERGY LIMITED</option>
                                            <option>PACIFIC OIL GHANA LIMITED</option>
                                            <option>PATRICK K.A BONNEY & CO. LIMITED</option>
                                            <option>PETRO AFRIQUE GHANA LTD</option>
                                            <option>PETROBAY OIL LIMITED</option>
                                            <option>PETROCELL LIMITED</option>
                                            <option>PETROLAND LIMITED</option>
                                            <option>PETROLEUM SOLUTIONS LIMITED</option>
                                            <option>PLUS ENERGY</option>
                                            <option>PUMA ENERGY GHANA LIMITED</option>
                                            <option>Q8 Oil(GH) COMPANY LIMITED</option>
                                            <option>QUANTUM PETROLEUM LIMITED</option>
                                            <option>RADIANCE PETROLEUM LIMITED</option>
                                            <option>READY OIL LIMITED</option>
                                            <option>RICH OIL COMPANY LIMITED</option>
                                            <option>RIEMA COMPANY LIMITED</option>
                                            <option>ROOTSENAF GAS COMPANY LIMITED</option>
                                            <option>ROYAL ENERGY COMPANY LIMITED</option>
                                            <option>ROYAL ROSES OIL COMPANY LIMITED</option>
                                            <option>RURAL ENERGY RESOURCES LIMITED (RUNEL)</option>
                                            <option>SAMA OIL</option>
                                            <option>SANTOL ENERGY LIMITED</option>
                                            <option>SAWIZ PETROLEUM COMPANY LIMITED</option>
                                            <option>SEAM OIL COMPANY LIMITED</option>
                                            <option>SEMANHYIA OIL LIMITED</option>
                                            <option>SEPHEM OIL COMPANY LIMITED</option>
                                            <option>SHAKAINAH VENTURES LIMITED</option>
                                            <option>SHELLEYCO PETROLEUM LIMITED</option>
                                            <option>SHEELM OIL</option>
                                            <option>SKY PETROLEUM </option>
                                            <option>SO ENERGY GH LIMITED</option>
                                            <option>SONNIDOM  LIMITED</option>
                                            <option>SPIRITS PETROLEUM LIMITED</option>
                                            <option>STAR OIL CO. LTD</option>
                                            <option>STRATEGIC ENERGIES LIMITED</option>
                                            <option>SUPERIOR OIL COMPANY LTD.</option>
                                            <option>TEL ENERGY LIMITED</option>
                                            <option>TOP OIL COMPANY LIMITED</option>
                                            <option>TOTAL PETROLEUM GHANA LIMITED</option>
                                            <option>THOMCOF ENERGY LIMITED</option>
                                            <option>TRADE CROSS LIMITED</option>
                                            <option>TRIGON ENERGY LIMITED</option>
                                            <option>TRINITY OIL COMPANY LIMITED</option>
                                            <option>TRIPLE A LP GAS LIMITED</option>
                                            <option>T- TEKPOR ENERGY</option>
                                            <option>UNION OIL GHANA LIMITED</option>
                                            <option>UNIQUE OIL COMPANY LTD.</option>
                                            <option>UNITY OIL COMPANY LIMITED</option>
                                            <option>UNIVERSAL OIL COMPANY LIMITED</option>
                                            <option>VENUS OIL COMPANY LIMITED</option>
                                            <option>VIRGIN PETROLEUM LTD</option>
                                            <option>VIVO ENERGY GHANA LIMITED</option>
                                            <option>WARREN OIL COMPANY LIMITED</option>
                                            <option>WEST AFRICAN PETROLEUM COMPANY (WAPCO)</option>
                                            <option>WORLD GAS COMPANY </option>
                                            <option>XPRESS GAS LIMITED</option>
                                            <option>YOKWA GAS LIMITED</option>
                                            <option>ZEN PETROLEUM LIMITED</option>
                                            <option>TOTAL</option>

                                        </select>

                                    </div>
                                </div> <!-- /control-group -->
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">OBs Litre</label>

                                    <div class="controls">
                                        <input type="number" class="span5" step="any" id="obslitreedit" name="obslitreedit" required>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Approve</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="deleteBdcModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete BDC</h4>
                        </div>
                        <form action="deletedailyapprove" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="iddelete" name="iddelete" />

                                <p>Are you sure u want to delete ?</p>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:#00ba8b;color: #fff" class="btn " >Delete</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main -->








<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="asset/js/toastr.min.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('click','.editbtn',function() {
        //alert("ok");
        $('#idedit').val($(this).data('id'));
        $('#vehiclenumberedit').val($(this).data('vehiclenumber'));
        $('#consignmentnameedit').val($(this).data('consignmentname'));
        $('#drivernameedit').val($(this).data('drivername'));
        $('#shoretankedit').val($(this).data('shoretank'));
        $('#ordernumberedit').val($(this).data('orderno'));

        $('#waybillnumberedit').val($(this).data('waybillno'));
        $('#customeredit').val($(this).data('customer')).select();
        $('#obslitreedit').val($(this).data('obslitre'));


        $('#temdegedit').val($(this).data('tempdeg'));
        $('#densityedit').val($(this).data('density'));
        $('#vcfedit').val($(this).data('vcf'));


    });
    $(document).on('click','.deletebtn',function() {
        $('#iddelete').val($(this).data('id'));

    });
</script>
</body>

</html>
