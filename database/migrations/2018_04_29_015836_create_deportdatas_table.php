<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeportdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deportdatas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user');
            $table->string('supervisor');
            $table->string('vehiclenumber');
            $table->string('drivername');
            $table->string('shoretank');
            $table->string('orderno');
            $table->string('waybillno');
            $table->string('customer');
            $table->string('obslitre');
            
            $table->string('tempdeg');
            $table->string('density');
            $table->string('vcf');
            
            $table->string('litre');
            $table->string('vac');
            $table->string('tonmetricair');
            
            $table->string('depotname');
            $table->string('consignmentname');
            $table->string('bank');
            $table->string('bdc');
            $table->string('status');
           
            
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deportdatas');
    }
}
