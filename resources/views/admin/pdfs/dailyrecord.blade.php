<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Daily Stock taking report</title>
    <style>
        #datarecord,#brieftable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #ff8533;
            color: black;
            font-size: 12px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }

</style>
</head>
<body>
<div>


    <div class="page">
        <h4><center><b>DAILY SUMMARY REPORT</b></center></h4>

          <img  src="img/ecm_logo.jpg" width="150px" height="60px" alt="logo" />

        <table id="brieftable" style="border: 1px solid black; width: 100%; " >
                      <tr >
                          <td>DATE :</td>
                          <td><b>{{date('d-m-Y')}}</b></td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr >
                          <td>COMPANY NAME :</td>
                          <td><b>{{$bdc}}</b></td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <td>DEPOT NAME :</td>
                          <td><b>{{$depotname}}</b></td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <td>DEPOT LOCATION :</td>
                          <td><b>{{$depotlocation}}</b></td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <td>PRODUCT :</td>
                          <td><b>{{$product}}</b></td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <td>DEPOT ATTENDANT :</td>
                          <td><b>{{$fieldrep}}</b></td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <td>ATTENDANT'S CONTACT DETAILS :</td>
                          <td style="border-right: 1px solid black;" ><b>{{$contact}}</b></td>
                          <td>GROSS METRIC TONS IN AIR :</td>
                          <td><b>{{number_format($gross_litres_in_air)}}</b></td>
                      </tr>
                      <tr>
                          <td>BANK :</td>
                          <td style="border-right: 1px solid black;"><b>{{$bank}}</b></td>
                          <td >GROSS LITRES IN TANK :</td>
                          <td><b>{{number_format($gross_litres_in_tank)}}</b></td>
                      </tr>
                  </table>
        <br>
        <table id="datarecord">
                     <thead>
                     <tr>
                         <th >VEHICLE NUMBER </th>
                         <th >DRIVERS NAME</th>
                         <th >SHORE TANK</th>
                         <th >ORDER NO</th>
                         <th >WAY BILL NO</th>
                         <th >CUSTOMER</th>
                         <th >OBS LITERS</th>
                         <th >OBS TEMP DEG C</th>
                         <th >DENSITY @ 15C</th>
                         <th >VCF T 54B</th>
                         <th >LITRE @15C</th>
                         <th >METRIC TONS VAC</th>
                         <th >METRIC TONS AIR</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php
                     $totalobslitres=0;$totallitre15=0;$vac=0;$metrictonair=0;?>
                     @foreach($dailyrecord as $s)
                         <?php
                         $totalobslitres=$totalobslitres + $s->obslitre;
                         $totallitre15=$totallitre15 +$s->litre;
                         $vac=$vac + $s->vac;
                         $metrictonair= $metrictonair + $s->tonmetricair;
                         ?>
                         <tr>
                             <td>{{$s->vehiclenumber}}</td>
                             <td>{{$s->drivername}}</td>
                             <td>{{$s->shoretank}}</td>

                             <td>{{$s->orderno}}</td>
                             <td>{{$s->waybillno}}</td>

                             <td>{{$s->customer}}</td>
                             <td>{{number_format($s->obslitre)}}</td>
                             <td>{{$s->tempdeg}}</td>
                             <td>{{$s->density}}</td>
                             <td>{{$s->vcf}}</td>
                             <td>{{number_format($s->litre)}}</td>
                             <td>{{sprintf('%0.3f', $s->vac)}}</td>
                             <td>{{sprintf('%0.3f', $s->tonmetricair)}}</td>
                         </tr>
                     @endforeach
                     <tbody>
                     <tfoot>
                     <tr>
                         <td><b>Total</b></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><b>{{number_format($totalobslitres)}}</b></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><b>{{number_format($totallitre15)}}</b></td>
                         <td><b>{{sprintf('%0.3f',$vac)}}</b></td>
                         <td><b>{{sprintf('%0.3f',$metrictonair)}}</b></td>
                     </tr>
                     </tfoot>
                 </table>

    </div>

     <div class="page">
         <h4><center><b>DAILY SUMMARY SHEET</b></center></h4>
        <table id="datarecord">
            <thead>
            <tr>
                <th >NUMBER </th>
                <th >CUSTOMER</th>
                <th >GROSS LITRES @ 15C</th>
                <th >LITRE @ 15C</th>
                <th >METRIC TONS VAC</th>
                <th >METRIC TONS AIR</th>
            </tr>
            </thead>
            <tbody>
            <?php  $totalObsLitres2=0;$totalLitre125=0;$vac2=0;$metrictonair2=0;$totalnumber=0;?>
            @foreach($summarysheet as $s)
                <?php
                $totalnumber=$totalnumber + $s->number;
                $totalObsLitres2=$totalObsLitres2 + $s->obslitre;
                $totalLitre125=$totalLitre125 + $s->litre;
                $vac2=$vac2 + $s->tonmetricair;
                $metrictonair2=$metrictonair2 + $s->tonmetricair;
                ?>
                <tr>
                    <td>{{$s->number}}</td>
                    <td>{{$s->customer}}</td>
                    <td>{{sprintf('%0.3f', $s->obslitre)}}</td>

                    <td>{{sprintf('%0.3f', $s->litre)}}</td>
                    <td>{{sprintf('%0.3f', $s->vac)}}</td>
                    <td>{{sprintf('%0.3f', $s->tonmetricair)}}</td>

                </tr>
            @endforeach
            <tbody>
            <tfoot>
            <tr>
                <td><b>{{$totalnumber}}</b></td>
                <td><b>GRAND TOTAL</b></td>
                <td><b>{{sprintf('%0.3f', $totalObsLitres2)}}</b></td>
                <td><b>{{sprintf('%0.3f',$totalLitre125)}}</b></td>
                <td><b>{{sprintf('%0.3f',$vac2)}}</b></td>
                <td><b> {{sprintf('%0.3f',$metrictonair2)}}</b></td>
            </tr>
            </tfoot>
        </table>

     </div>


</div>


</body>
</html>