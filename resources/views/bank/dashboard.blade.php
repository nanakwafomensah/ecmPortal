<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">
    <link href="asset/css/style.css" rel="stylesheet">
    <link href="asset/css/pages/dashboard.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html">ECM PORTAL-{{$bank}}    &nbsp;<h5><i class="icon-user"></i> Hello,{{Sentinel::getUser()->first_name}}</h5></a>
            <div class="nav-collapse">
                <ul class="nav pull-right">


                    @if(Sentinel::check())

                        <form action="{{route('logout')}}" method="post" id="logout-form">
                            {{csrf_field()}}
                            <a class="btn" style="color: darkred;background-color: white;" href="#" onclick="document.getElementById('logout-form').submit()" style="color: black"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>

                        </form>


                    @endif

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /container -->
    </div>
    <!-- /navbar-inner -->
</div>

<!-- /navbar -->
<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">
                <li class="active"><a href="bankindex"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
                <li ><a href="reportbank"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
                <li class=""><a href="bankreprecord"><i class="icon-list-alt"></i><span>Field Records</span> </a> </li>
                {{--<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Data Entry</span> <b class="caret"></b></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="bdc">BDC</a></li>--}}
                        {{--<li><a href="deport">DEPORT</a></li>--}}
                        {{--<li><a href="bank">BANK</a></li>--}}
                        {{--<li><a href="constant">CONSTANT</a></li>--}}
                        {{--<li><a href="user">USERS</a></li>--}}

                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li ><a href="consignment"><i class="icon-list-alt"></i><span>New Consignment</span> </a> </li>--}}
                {{--<li><a href="report"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>--}}

            </ul>
        </div>
        <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
</div>
<!-- /subnavbar -->
<div class="main">
    <div class="main-inner">
        <div class="container">
            <?php
            $i=1;
            $status=null;
            ?>
@foreach($consignmnent as $con)

            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="alert " role="alert" style="border-color: darkred;background-color: white" >
                              <span class="badge badge-light" style="background-color: white;color:black">{{$i++}}</span>
                                <?php if($con->status=="open"){
                                    $status="Ongoing";
                                }else{
                                    $status="Closed";
                                }?>
                               <a data-toggle="collapse" href="#{{$con->reference_number}}" style="color:darkred;">{{strtoupper($con->consignmentname)}}</a>&nbsp;&nbsp;<a href="#" class="badge badge-danger" style="background-color: darkred">{{$status}}</a>
                            </div>

                        </h4>
                    </div>
                    <div id="{{$con->reference_number}}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <center>
                                <table  class="table table-striped">
                                    <tr>
                                        <th scope="row">REFERENCE NUMBER</th>
                                        <th>SUPPLIER</th>

                                        <th>PRODUCT</th>
                                        <th>DEPOT </th>
                                        <th>TOTAL QUANTITY</th>
                                        <th>RECORDED</th>
                                    </tr>
                                    <tr>
                                        <td scope="row">{{strtoupper($con->reference_number)}}</td>
                                        <td>{{strtoupper($con->supplier)}}</td>

                                        <td>{{strtoupper($con->product)}}</td>
                                        <td>{{strtoupper($con->depot)}}</td>
                                        <td>{{strtoupper($con->totalquantity)}}</td>
                                        <td>{{isset($con->totalrecorded) ? $con->totalrecorded : 0 }}</td>
                                    </tr>

                                </table>
                                <br>
                                <a href="downloadpdfreportbank/{{$con->reference_number}}/{{$con->bank}}" class="btn " style="background-color: darkred;color: white"> Download PDF</a>
                                |
                                <a  href="downloadExcelreportbank/csv/{{$con->reference_number}}/{{$con->bank}}" class="btn " style="background-color: darkred;color: white"> Download Excel</a>
                            </center>

                        </div>

                    </div>
                </div>
            </div>
    <hr style=" display: block;height: 2px;border: 0;border-top: 2px solid white;margin: 1em 0;padding: 0; ">

@endforeach



        </div>
        <!-- /container -->
    </div>
    <!-- /main-inner -->
</div>
</div>
<!-- /main -->

<!-- /extra -->
{{--{{include('include/footer')}}--}}
        <!-- /footer -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="asset/js/jquery-1.7.2.min.js"></script>
<script src="asset/js/excanvas.min.js"></script>
<script src="asset/js/chart.min.js" type="text/javascript"></script>
<script src="asset/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="asset/js/full-calendar/fullcalendar.min.js"></script>

<script src="asset/js/base.js"></script>


</body>
</html>
