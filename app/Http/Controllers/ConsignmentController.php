<?php

namespace App\Http\Controllers;

use App\Consignment;
use App\Contant;
use Illuminate\Http\Request;
use DB;
use Sentinel;
class ConsignmentController extends Controller
{
    //
    
    public  function index(){
        $bdcs=DB::table('ddcs')->get();
        $banks=DB::table('banks')->get();
        $depots=DB::table('depots')->get();
        $consignments=DB::table('consignments')->get();
        $mainconsignments=DB::table('mainconsignments')->get();

        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();
        return view("admin/entry/consignment")->with([
            'bdcs'=>$bdcs ,
            'banks'=>$banks,
            'depots'=>$depots,
            'consignments'=>$consignments,
            'mainconsignments'=>$mainconsignments,
            'constants'=>$constants,
            'consignment'=>$consignment
            
        ]);
    }
  
    public function switchconsignmentstate($id,$state){
        $currentStatus=null;
        if($state =='open'){$currentStatus='close';}elseif ($state =='close'){ $currentStatus='open';}
        try{
            $m = Consignment::findorfail($id);
            $m->status=$currentStatus;
             if($m->update()){
                 $notification=array(
                    'message'=>"Consignment Status Updated Successfully",
                    'alert-type'=>'success'
                 );
                return redirect('consignment')->with($notification);
               }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Consignment Status Failed to update",
                'alert-type'=>'success'
            );
            return redirect('consignment')->with($notification);
        }
    }
    public function save(Request $request){
        
        try{
            $m = new Consignment();
            $m->reference_number=$request->referenceNumber;
            $m->supplier=$request->supplier;
            $m->date_issued=$request->date_issued;
            $m->date_expire=$request->date_expire;
            $m->totalquantity=$request->totalquantity;
            $m->description=$request->description;
            $m->port_of_discharge=$request->port_of_discharge;
            $m->product=$request->product;
            $m->depot=$request->depot;

            $m->bank=$request->bank;
            $m->bdc=$request->bdc;
            $m->consignmentname=$request->consignmentname;
            $m->status="open";
            $m->contact=$request->contact;


            $sender = "ECM";
            $message = "Record taking on a new consignment with LC Number $request->referenceNumber has been initiated";
            $receipient = preg_replace('/^0/','233',$request->contact);
           
            $msg = urlencode($message);
            $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $receipient . '&source=' . $sender . '&message=' . $msg;
            file_get_contents($api);

            if($m->save()){
                $notification=array(
                    'message'=>"New Consigment  has been Succesfully Added",
                    'alert-type'=>'success'
                );

                $z = new Contant();
                $z->temDeg=0;
                $z->density=0;
                $z->vcf=0;
                $z->consignmentid=$request->consignmentname;
                $z->save();


                return redirect('consignment')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('consignment')->with($notification);
        } 
    }
    public function update(Request $request){
        try{
            $m = Consignment::findorfail($request->idedit);
            $m->reference_number=$request->referenceNumber_edit;
            $m->supplier=$request->supplier_edit;
            $m->date_issued=$request->date_issued_edit;
            $m->date_expire=$request->date_expire_edit;
            $m->totalquantity=$request->totalquantity_edit;
            $m->description=$request->description_edit;
            $m->port_of_discharge=$request->port_of_discharge_edit;
            $m->product=$request->product_edit;
            $m->depot=$request->depot_edit;

            $m->bank=$request->bank_edit;
            $m->bdc=$request->bdc_edit;

            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('consignment')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Consigment  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('consignment')->with($notification);
        }
    }
    public function delete(Request $request){

        try{
            $m = Consignment::findorfail($request->iddelete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Consigment has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('consignment')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('consignment')->with($notification);
        }
    }
}
