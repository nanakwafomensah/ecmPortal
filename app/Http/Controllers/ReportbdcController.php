<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use PDF;
use DB;
use Sentinel;
class ReportbdcController extends Controller
{
    //
    //
    public function index(){
        $roleFieldrep = Sentinel::findRoleBySlug('Fieldrep');
        $rolesupervisor = Sentinel::findRoleBySlug('Supervisor');
        $users = $roleFieldrep->users()->with('roles')->get();
        $usersSupervisors = $rolesupervisor->users()->with('roles')->get();


        $bdcs=DB::table('ddcs')->get();
        $depots=DB::table('depots')->get();
        $banks=DB::table('banks')->get();
        $consignments=DB::table('consignments')->get();
        $fieldreps=$users;
        $supervisorrep=$usersSupervisors;

        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();

        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();

        return view('bdc.reportbdc')->with([
            'depots'=>$depots,
            'bdcs'=>$bdcs,
            'banks'=>$banks,
            'consignments'=>$consignments,
            'fieldreps'=>$fieldreps,
            'supervisorrep'=>$supervisorrep,
            'constants'=>$constants,
            'consignment'=>$consignment,
        ]);
    }
}
