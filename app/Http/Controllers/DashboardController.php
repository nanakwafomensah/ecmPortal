<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class DashboardController extends Controller
{
    //
    public function index(){
        $todaydate=date('Y-m-d');
     
        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();
     
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();
        //->where('deportdatas.status','invalivalid')

        $depouserrecord= DB::table('deportdatas')

            ->join('users', 'users.name', '=', 'deportdatas.user')
            ->select( DB::raw('users.name,users.consignmentname,COUNT(deportdatas.id) as number'))
            ->groupBy('users.name','users.consignmentname','deportdatas.id')
            ->whereDate('deportdatas.created_at','=',$todaydate)
            ->get();
       // dd($depouserrecord);


        $users=DB::table('users')->count();
        $consignments=DB::table('consignments')->count();
        $depots=DB::table('depots')->count();
        $bdcs=DB::table('ddcs')->count();
        $banks=DB::table('banks')->count();

        return view('admin/dashboard/dashboard')->with([
            'constants'=>$constants,
            'nousers'=>$users,
            'noconsignments'=>$consignments,
            'nodepots'=>$depots,
            'nobdcs'=>$bdcs,
            'nobanks'=>$banks,
            'consignment'=>$consignment,
            'depouserrecord'=>$depouserrecord,
        ]);

    }
}
