<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Sentinel;
class BankdashboardController extends Controller
{
    //
    public  function  index(){
        $bank=trim(Sentinel::getUser()->bank);

        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select('consignments.*', DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.bank',$bank)
            ->where('deportdatas.status','valid')
            ->get();
        if($consignment->isEmpty()){
            $consignment= DB::table('consignments')->select('consignments.*')->where('consignments.bank',$bank)->get();
        }
       
        $bankname= DB::table('consignments')->select([DB::raw('bank')])->where('bank',$bank)->first();

        return view('bank.dashboard')->with([
            'consignmnent'=>$consignment,
            'bank'=>$bankname->bank

        ]);
    }

}
