<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ecm</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

    <link href="asset/css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet" type="text/css">
    <link href="asset/css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body style="background-image: url(asset/img/frontimage.jpg); ">

<div class="navbar navbar-fixed-top" >

    <div class="navbar-inner" >

        <div class="container" >

            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <a class="brand" href="http://8.9.4.232">
               ECM - PORTAL
            </a>



        </div> <!-- /container -->

    </div> <!-- /navbar-inner -->

</div> <!-- /navbar -->



<div class="account-container" >

    <div class="content clearfix">

        <form action="login" method="post">

            <h3><center>ACCOUNT LOGIN</center></h3>

            <div class="login-fields">

                {{csrf_field()}}
                @if(session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Email" name="email">
                    </div>
                </div>
<br>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Password" name="password">
                    </div>
                </div>



            </div> <!-- /login-fields -->

            <div class="login-actions">

                <button class="button btn  btn-large" style="background-color: darkred;color:white" type="submit">Sign In</button>

            </div> <!-- .actions -->



        </form>

    </div> <!-- /content -->

</div> <!-- /account-container -->



{{--<div class="login-extra">--}}
    {{--<a href="#">Reset Password</a>--}}
{{--</div> <!-- /login-extra -->--}}


<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>
