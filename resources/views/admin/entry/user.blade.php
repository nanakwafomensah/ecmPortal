<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="asset/css/dataTable.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />




</head>

<body>
@include('layout.admin.navbarToplayout')
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget">

                        <div class="widget-header" style="background: white !important; ">
                            <i class="icon-user"  style="color:darkred !important;"></i>
                            <h3 style="color:darkred !important;">User</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->
                    <button class="btn btn-lg" style="background-color:darkred;color: #fff" data-toggle="modal" data-target="#addbankModal">Add New</button><span>&nbsp;</span>


                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>First Name</th>
                            <th>Last Name</th>

                            <th>Email</th>
                            <th>Priviledge</th>
                            <th>Consignments Assigned</th>


                            <th>Action</th>

                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>First Name</th>
                            <th>Last Name</th>

                            <th>Email</th>
                            <th>Priviledge</th>
                            <th>Consignments Assigned</th>
                            <th>Action</th>

                        </tr>
                        </tfoot>
                        <?php
                        $i=1;
                        ?>
                        @foreach($userdetails as $userdetails)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$userdetails->first_name}}</td>
                                <td>{{$userdetails->last_name}}</td>
                                <td>{{$userdetails->email}}</td>
                                <td>
                                    <span class="badge badge-pill badge-danger">{{$userdetails->priviledge}}</span>{{'['.$userdetails->depot.']'}}
                                </td>
                                <td>{{$userdetails->consignmentname}}</td>

                                <td>
                                    <div class="controls">
                                        <div class="btn-group">
                                            <a class="btn " style="background-color:white;color: darkred" href="#"><i class="icon-action icon-white"></i>Select</a>
                                            <a class="btn  dropdown-toggle" style="background-color:darkred;color: #fff" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-toggle="modal"  data-target="#editbankModal" class="edit-modal" data-id="{{$userdetails->id}}"  data-firstname="{{$userdetails->first_name}}" data-lastname="{{$userdetails->last_name}}"  data-priviledge="{{$userdetails->priviledge}}"  data-email="{{$userdetails->email}}" data-depot="{{$userdetails->depot}}" data-bank="{{$userdetails->bank}}" data-bdc="{{$userdetails->bdc}}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a data-toggle="modal"  data-target="#deletebankModal" class="deletebtn"  data-id="{{$userdetails->id}}" data-firstname="{{$userdetails->first_name}}" data-lastname="{{$userdetails->last_name}}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>	<!-- /controls -->
                                </td>

                            </tr>
                            @endforeach
                            </tbody>
                    </table>


                </div> <!-- /widget -->

            </div> <!-- /span8 -->



            <!-- Modal -->
            <div id="addbankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">New User</h4>
                        </div>
                        <form action="adduser" method="post">
                            <div class="modal-body">

                                <div><h4>USER BASIC DETAILS</h4></div>
                                        <div class="col-md-6">
                                            <div class="control-group">
                                                <label for="field-1" class="control-label">Firstname</label>
                                                <input type="text" class="span5" id="field-1" placeholder="John" name="firstname" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-2" class="control-label">lastname</label>
                                                <input type="text"class="span5" id="field-2" placeholder="Mensah" name="lastname" required>
                                            </div>
                                        </div>



                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">Email</label>
                                                <input type="email" class="span5" id="field-3" placeholder="me12@yahoo.com" name="email" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Phone number</label>
                                                <input type="text" class="span5" id="field-4" placeholder="0202323222" name="tel" required>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Role</label>
                                                <select id="userRole" name="role" class="span5" required>
                                                    <option value="">Select Role</option>
                                                    <option value="Admin">Admin</option>
                                                    <option value="Fieldrep">Fieldrep</option>
                                                    <option value="Supervisor">Supervisor</option>
                                                    <option value="Bdcrep">bdcrep</option>
                                                    <option value="Bankrep">bankrep</option>

                                                </select>
                                            </div>
                                        </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Password</label>
                                        <input type="password" class="span5" id="field-5" placeholder="********" name="password" required>
                                    </div>
                                </div>
                                <hr style=" display: block;height: 2px;border: 0;border-top: 2px solid darkred;margin: 1em 0;padding: 0; ">
                                <div class="hideforAdmin">
                                 <div><h4>USER ASSIGNMENT</h4></div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="field-1" class="control-label">Assign FieldRep to a Consignment</label>
                                        <select name="consignmentname[]" class="span5"  multiple>
                                            <option value="">Select Option</option>
                                            @foreach($consignments as $con)
                                                <option>{{$con->consignmentname}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label"> Assign To A Depot</label>
                                        <select name="depot" class="span5" >
                                            <option value="">Select Option</option>
                                            @foreach($depots as $depot)
                                            <option value="{{$depot->name}}">{{$depot->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Assign To  A Bank</label>
                                        <select name="bank" class="span5" >
                                            <option value="">Select Option</option>
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->name}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Assign To A BDC</label>
                                        <select name="bdc" class="span5" >
                                            <option value="">Select Option</option>
                                            @foreach($bdcs as $bdc)
                                                <option value="{{$bdc->name}}">{{$bdc->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                </div>



                                </div>



                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="editbankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit User</h4>
                        </div>
                        <form action="edituser" method="post">
                            <div><h4>USER BASIC DETAILS</h4></div>
                            <div class="modal-body">
                                <input type="hidden" id="id_edit" name="id_edit" />

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Firstname</label>
                                            <input type="text" class="span5"  id="firstname_edit" name="firstname_edit">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">lastname</label>
                                            <input type="text" class="span5"  id="lastname_edit" name="lastname_edit">
                                        </div>
                                    </div>
                                       <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Email</label>
                                            <input type="text" class="span5" id="email_edit"  name="email_edit">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Role</label>
                                            <select name="role_edit" class="span5" id="role_edit" >
                                                <option value="Admin">Admin</option>
                                                <option value="Fieldrep">Fieldrep</option>
                                                <option value="Supervisor">Supervisor</option>
                                                <option value="Bdcrep">Bdcrep</option>
                                                <option value="Bankrep">Bankrep</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Password</label>
                                        <input type="password" class="span5"  id="password_edit"  name="password_edit" required>
                                    </div>
                                </div>
                                <div class="hideforAdmin">
                                <hr style=" display: block;height: 2px;border: 0;border-top: 2px solid darkred;margin: 1em 0;padding: 0; ">
                                <div><h4>USER ASSIGNMENT</h4></div>

                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="field-1" class="control-label">Consignment</label>
                                        <select name="consignmentnameedit[]" class="span5" multiple>
                                            @foreach($consignments as $con)
                                                <option>{{$con->consignmentname}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Depot</label>
                                        <select name="depotedit" id="depot_edit" class="span5">
                                            @foreach($depots as $depot)
                                                <option value="{{$depot->name}}">{{$depot->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Assign To  A Bank</label>
                                        <select name="editbank" class="span5" id="bank_edit" required>
                                            <option value="">Select Option</option>
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->name}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Assign To A BDC</label>
                                        <select name="editbdc" class="span5" id="bdc_edit" required>
                                            <option value="">Select Option</option>
                                            @foreach($bdcs as $bdc)
                                                <option value="{{$bdc->name}}">{{$bdc->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


</div>


                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save Changes</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="deletebankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete User</h4>
                        </div>
                        <form action="deleteuser" method="post">
                            <div class="modal-body">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Are you sure you want to delete <span id="name_delete"></span>?</label>
                                            <input type="hidden" class="form-control" id="id_delete" placeholder="John" name="id_delete" >
                                        </div>
                                    </div>


                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Delete</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main -->







<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>
<script>
    $(function() {
        $('.hideforAdmin').hide();
    });
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="asset/js/dataTable.js"></script>
<script src="asset/js/toastr.min.js"></script>
<script src="asset/js/utility.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('click','.edit-modal',function() {
        // alert($(this).data('id'));
        $('#id_edit').val($(this).data('id'));
        $('#firstname_edit').val($(this).data('firstname'));
        $('#lastname_edit').val($(this).data('lastname'));
        $('#username_edit').val($(this).data('username'));

        $('#email_edit').val($(this).data('email'));
        $('#role_edit').val($(this).data('priviledge')).change();
        $('#depot_edit').val($(this).data('depot')).change();
        $('#bank_edit').val($(this).data('bank')).change();
        $('#bdc_edit').val($(this).data('bdc')).change();



    });
</script>
<script>
    $(document).on('click','.deletebtn',function() {
//        alert("Hey");

        $('#id_delete').val($(this).data('id'));


        $("#name_delete").html($(this).data('firstname')+""+$(this).data('lastname'));

    });
</script>
<script>
    $(document).on('change','#userRole',function(){
         var userRole=$(this).val();
        if(userRole == "Admin"){
           $('.hideforAdmin').hide();
        }else {
            $('.hideforAdmin').show();
        }
    });

    $(document).on('change','#role_edit',function(){
         var userRole=$(this).val();
        if(userRole == "Admin"){
           $('.hideforAdmin').hide();
        }else {
            $('.hideforAdmin').show();
        }
    });
</script>
</body>

</html>
