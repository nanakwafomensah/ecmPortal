<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ddc;
use DB;
class BdcController extends Controller
{
    //
    public function index(){
        $bdcs=DB::table('ddcs')->get();
        $banks=DB::table('banks')->get();
        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();
        
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();
        return view("admin/entry/bdc")->with([
            'bdcs'=>$bdcs ,
            'banks'=>$banks,
            'constants'=>$constants,
            'consignment'=>$consignment
        ]);
    }

    public function save( Request $request){

        try{
            $m = new Ddc();

            $m->name=$request->name;
            $m->address=$request->address;
            $m->phonenumber=$request->phonenumber;
            $m->bankname=$request->bankname;

            if($m->save()){
                $notification=array(
                    'message'=>"New Bdc  has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('bdc')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('bdc')->with($notification);
        }
    }

    public function update(Request $request){

        try{
            $m = Ddc::findorfail($request->idedit);
            $m->name=$request->nameedit;
            $m->address=$request->addressedit;
            $m->phonenumber=$request->phonenumberedit;
            $m->bankname=$request->banknameedit;


            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('bdc')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('bdc')->with($notification);
        }
    }
    public function delete(Request $request){
        try{
            $m = Ddc::findorfail($request->iddelete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Bdc has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('bdc')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('bdc')->with($notification);
        }
    }
}
