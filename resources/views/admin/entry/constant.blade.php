<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
@include('layout.admin.navbarToplayout')
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header " style="background: white !important; ">
                            <i class="icon-user" style="color:darkred"></i>
                            <h3 style="color:darkred">Contants</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->
                    <div>
                        <form action="updateconstant" method="post">
                            <div class="modal-body">
                               {{--<input type="hidden" id="id" name="id" value="{{$constants->id}}" />--}}

                                <div class="control-group">
                                    <label class="control-label" for="firstname">Consignment Name</label>
                                    <div class="controls">
                                       <select name="consignmentname" id="consignmentname" class="span5">
                                           <option value="">Select an option</option>
                                           @foreach($consignments as $con)
                                               <option value="{{$con->consignmentname}}">{{$con->consignmentname}}</option>

                                           @endforeach
                                       </select>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Obs Temp Deg Celsius</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="temDeg" name="temDeg"   required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Density @ 15C</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="density" name="density"   required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">VCF T 54B</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="vcf" name="vcf"   required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Updated Date</label>
                                    <div class="controls">
                                        <input type="date" class="span5" id="phonenumber" name="updated_at" required>
                                    </div> <!-- /controls -->
                                </div>

                            </div>
                            <div style="float:right">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>


                </div> <!-- /widget -->

            </div> <!-- /span8 -->



            <!-- Modal -->


        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main -->







<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>


<script src="asset/js/toastr.min.js"></script>
<script src="asset/js/utility.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('change','#consignmentname',function(e) {
       var consignmentname=$(this).val();
          $.ajax({
            type: 'POST',
            url: '{{URL::to('getConstant')}}',
            data: {
                '_token': "{{ csrf_token() }}",
                'consignmentname':consignmentname
             },
            success: function (data) {
                var obj = JSON.parse(data);
                $('#density').val(obj.density);
                $('#temDeg').val(obj.temDeg);
                $('#vcf').val(obj.vcf);
            }
        })
    });
</script>

</body>

</html>
