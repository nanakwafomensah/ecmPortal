<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_number');
            $table->string('supplier');
            $table->date('date_issued');
            $table->date('date_expire');
            $table->double('totalquantity', 10, 5);
            $table->text('description');
            $table->string('port_of_discharge');
            $table->string('product');
            $table->string('depot');
            $table->string('bank');
            $table->string('bdc');
            $table->string('consignmentname');
            $table->string('contact');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consignments');
    }
}
