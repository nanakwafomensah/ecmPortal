<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Daily Stock taking report</title>
    <style>
        #datarecord,#brieftable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #ff8533;
            color: black;
            font-size: 12px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }

    </style>
</head>
<body >
<div >
    <div class="page">
    <h4 style="font-family: 'Trebuchet MS, Arial, Helvetica, sans-serif';"><center><b>CUMMULATIVE STOCK REPORT</b></center></h4>


    <img style="float: right;" src="img/ecm_logo.jpg" width="110px" height="40px" alt="logo" />
    <div>
        <table id="brieftable" style="border: 1px solid black; width: 100%; " >
            <tr >
                <td>DATE :</td>
                <td><b>{{date('d-m-Y')}}</b></td>
                <td></td>
                <td></td>
            </tr>
            <tr >
                <td>COMPANY NAME :</td>
                <td><b>{{$bdc}}</b></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>DEPOT NAME :</td>
                <td><b>{{$depotname}}</b></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>DEPOT LOCATION :</td>
                <td><b>{{$depotlocation}}</b></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>PRODUCT :</td>
                <td><b>{{$product}}</b></td>
                <td></td>
                <td></td>
            </tr>
            {{--<tr>--}}
                {{--<td>DEPOT SUPERVISOR :</td>--}}
                {{--<td><b>{{$supervisor}}</b></td>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
            {{--</tr>--}}
            <tr>
                <td></td>
                <td style="border-right: 1px solid black;" ></td>
                <td>GROSS METRIC TONS IN AIR :</td>
                <td><b>{{number_format($gross_litres_in_air)}}</b></td>
            </tr>
            <tr>
                <td>BANK :</td>
                <td style="border-right: 1px solid black;"><b>{{$bank}}</b></td>
                <td >GROSS LITRES IN TANK :</td>
                <td><b>{{number_format($gross_litres_in_tank)}}</b></td>
            </tr>
        </table>
    </div>
    <br>

    <div>
        <div>

            @foreach($allDate as $d)

                <table id="datarecord" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>DATE </th>
                        <th>ORDER NO</th>
                        <th>WAY BILL NUMBER</th>
                        <th>CUSTOMER</th>
                        <th>OBS LITERS</th>
                        <th>OBS TEMP DEG C</th>
                        <th>DENSITY @ 15C</th>
                        <th>VCF T 54B</th>
                        <th>LITRE @ 15C</th>
                        <th>METRIC TONS VAC</th>
                        <th>METRIC TONS AIR</th>
                        <th>DAILY LOADING</th>
                        <th>OPENING QUANTITY</th>
                        <th>CLOSING QUANTITY</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $totalObsLitres=0;$totalLitre15=0;$vac=0;$metrictonair=0;
                    $totalrecorded=0; $count =0;
                    $totalopeningquantity=0;
                    $totalclosingquantity=0;
                    ?>
                    @foreach($dailyrecord as $s)

                        <?php
                        $remainder=0;
                        $totalrecorded ++;
                        $count++;
                        $newclosingquantity;

                        $totalObsLitres=$totalObsLitres + $s->litre; $totalLitre15=$totalLitre15 + $s->litre;
                        $vac=$vac + $s->tonmetricair;$metrictonair=$metrictonair + $s->tonmetricair;
                         if(strtok($s->created_at, ' ') == $d->dates ){
                                $consignmentDate=date('d-M-Y',strtotime($initialConsignmentInfo->date));
                                $totalQuantity=$initialConsignmentInfo->totalquantity;


                        ?>
                        <tr>
                            <td>{{date('d-M-Y',strtotime($s->created_at))}}</td>
                            <td>{{$s->orderno}}</td>
                            <td>{{$s->waybillno}}</td>

                            <td>{{$s->customer}}</td>
                            <td>{{number_format($s->obslitre)}}</td>

                            <td>{{$s->tempdeg}}</td>
                            <td>{{$s->density}}</td>
                            <td>{{$s->vcf}}</td>
                            <td>{{sprintf('%0.3f', $s->litre)}}</td>
                            <td>{{sprintf('%0.3f', $s->vac)}}</td>
                            <td>{{sprintf('%0.3f ', $s->tonmetricair)}}</td>
                            <td></td>
                            <td>
                                       @if($count == 1 )
                                         {{number_format($totalQuantity)}}
                                       @else
                                           {{$newclosingquantity}}
                                      @endif

                            </td>
                            <td>

                            </td>
                        </tr>
                       <?php  }
                    $newclosingquantity=sprintf('%0.3f',$totalQuantity - $metrictonair);
                    ?>

                    @endforeach
                    <tbody>
                    <tfoot>
                    <tr >
                        <td>Total</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{number_format( $totalObsLitres)}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{sprintf('%0.3f',$totalLitre15)}}</td>
                        <td>{{sprintf('%0.3f',$vac)}}</td>
                        <td>{{sprintf('%0.3f',$metrictonair)}}</td>
                        <td>{{sprintf('%0.3f',$metrictonair)}}</td>
                        <td></td>
                        <td>{{ sprintf('%0.3f',$totalQuantity - $metrictonair) }}</td>


                    </tr>
                    </tfoot>

                </table>
                <br>
                 <?php

                $totalObsLitres=0;$totalLitre15=0;$vac=0;$metrictonair=0;
                $newclosingquantity=$totalQuantity - sprintf('%0.3f',$metrictonair);
                ?>
                @endforeach

        </div>



    </div>
    </div>
    <div class="page">
        <center><h4 style="font-family: 'Trebuchet MS, Arial, Helvetica, sans-serif';">ANALYSIS OF CUSTOMER TOTAL LIFTINGS OR LOADINGS</h4></center>
        <table id="datarecord" style="width: 100%;">
            <thead>
            <tr>
                <th>NUMBER </th>
                <th>CUSTOMER</th>
                <th>GROSS LITRES</th>
                <th>LITRE @ 15C</th>
                <th>METRIC TON VAC</th>
                <th>METRIC TON AIR</th>


            </tr>
            </thead>
            <tbody>
            <?php  $totalObsLitres2=0;$totalLitre125=0;$vac2=0;$metrictonair2=0;$totalnumber=0;?>
            @foreach($dailyrecord2 as $s)
                <?php
                $totalnumber=$totalnumber + $s->number;
                $totalObsLitres2=$totalObsLitres2 + $s->obslitre;
                $totalLitre125=$totalLitre125 + $s->litre;
                $vac2=$vac2 + $s->tonmetricair;
                $metrictonair2=$metrictonair2 + $s->tonmetricair;
                ?>
            <tr>
                <td>{{$s->number}}</td>
                <td>{{$s->customer}}</td>
                <td>{{sprintf('%0.3f', $s->obslitre)}}</td>
                <td>{{sprintf('%0.3f', $s->litre)}}</td>
                <td>{{sprintf('%0.3f', $s->vac)}}</td>
                <td>{{sprintf('%0.3f', $s->tonmetricair)}}</td>

            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr >
                <td>{{$totalnumber}}</td>
                <td>GRAND TOTAL</td>
                <td>{{sprintf('%0.3f', $totalObsLitres2)}}</td>
                <td>{{sprintf('%0.3f',$totalLitre125)}}</td>
                <td>{{sprintf('%0.3f',$vac2)}}</td>
                <td>{{sprintf('%0.3f',$metrictonair2)}}</td>



            </tr>
            </tfoot>

        </table>
    </div>
</div>


</body>
</html>