<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
          rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">
    <link href="asset/css/style.css" rel="stylesheet">
    <link href="asset/css/pages/dashboard.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>


<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html">ECM PORTAL    &nbsp;<h5><i class="icon-user"></i> Hello,{{Sentinel::getUser()->first_name}}</h5></a>
            <div class="nav-collapse">
                <ul class="nav pull-right">


                    @if(Sentinel::check())

                        <form action="{{route('logout')}}" method="post" id="logout-form">
                            {{csrf_field()}}
                            <a class="btn" style="color: darkred;background-color: white;" href="#" onclick="document.getElementById('logout-form').submit()" style="color: black"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>

                        </form>


                    @endif

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /container -->
    </div>
    <!-- /navbar-inner -->
</div>
<!-- /navbar -->
<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">
                <li class=""><a href="bankindex"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
                <li class="active"><a href="reportbank"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
                <li class=""><a href="bankreprecord"><i class="icon-list-alt"></i><span>Field Records</span> </a> </li>
            </ul>
        </div>
        <!-- /container -->
    </div>

    <!-- /subnavbar-inner -->
</div>
<!-- /subnavbar -->



<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header">

                            <h3>Your Reports</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">



                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#dailystock" data-toggle="tab" style="color:darkred;">DAILY STOCK TAKING</a>
                                    </li>
                                    <li >
                                        <a href="#dailysummary" data-toggle="tab" style="color:darkred;">DAILY SUMMARY </a>
                                    </li>
                                    <li >
                                        <a href="#cummulativestock" data-toggle="tab" style="color:darkred;">CUMMULATIVE STOCK TAKING</a>
                                    </li>
                                </ul>

                                <br>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="dailystock">
                                        <form id="edit-profile" class="form-horizontal" action="dailyreportrptbank" method="post">
                                            {{csrf_field()}}
                                            <fieldset>
                                                <input class="form-control" name="typereport" type="hidden" value="1"/>
                                                <div class="control-group">
                                                    <label class="control-label" >Consignment Name :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" id="consignmentdrop1" name="consignmentname" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($consignments as $consignment)
                                                                    <option value="{{$consignment->consignmentname}}">{{$consignment->consignmentname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->

                                                {{--<div class="control-group">--}}
                                                    {{--<label class="control-label" for="radiobtns">Field Representative Name :</label>--}}

                                                    {{--<div class="controls">--}}
                                                        {{--<div class="btn-group">--}}
                                                            {{--<a class="btn btn-default" href="#"> Select</a>--}}

                                                            {{--<select class="form-control" name="fieldrep" required>--}}
                                                                {{--<option value="">Select Option</option>--}}
                                                                {{--@foreach($fieldreps as $fieldrep)--}}
                                                                    {{--<option value="{{trim($fieldrep->name)}}">{{trim($fieldrep->name)}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>	<!-- /controls -->--}}
                                                {{--</div> <!-- /control-group -->--}}


                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Depot :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" id="depot1" name="depotname" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($depots as $depot)
                                                                    <option value="{{$depot->name}}">{{$depot->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Company Name(BDC) :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" > Select</a>

                                                            <select class="form-control" name="bdc" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($bdcs as $bdc)
                                                                    <option value="{{$bdc->name}}">{{$bdc->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->

                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Bank :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" name="bank" required >

                                                                <option selected value="{{$banks}}">{{$banks}}</option>

                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="firstname">Date From</label>
                                                    <div class="controls">
                                                        <input type="date" name="startdate" class="span3" id="datefrom" required>
                                                    </div> <!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="lastname">Date To</label>
                                                    <div class="controls">
                                                        <input type="date" name="enddate" class="span3" id="datefrom" required>
                                                    </div> <!-- /controls -->
                                                </div> <!-- /control-group -->

                                                <br />


                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-primary" style="background-color: darkred">Download PDF</button>

                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>

                                    <div class="tab-pane" id="dailysummary">
                                        <form id="edit-profile2" class="form-vertical" action="dailyreportrptbank" method="post">
                                            {{csrf_field()}}
                                            <fieldset>

                                                <input class="form-control" name="typereport" type="hidden" value="2"/>
                                                <div class="control-group">
                                                    <label class="control-label" >Consignment Name :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" id="consignmentdrop2" name="consignmentname" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($consignments as $consignment)
                                                                    <option value="{{$consignment->consignmentname}}">{{$consignment->consignmentname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->

                                                {{--<div class="control-group">--}}
                                                    {{--<label class="control-label" for="radiobtns">Field Representative Name :</label>--}}

                                                    {{--<div class="controls">--}}
                                                        {{--<div class="btn-group">--}}
                                                            {{--<a class="btn btn-default" href="#"> Select</a>--}}

                                                            {{--<select class="form-control" name="fieldrep" required>--}}
                                                                {{--<option value="">Select Option</option>--}}
                                                                {{--@foreach($fieldreps as $fieldrep)--}}
                                                                    {{--<option value="{{trim($fieldrep->name)}}">{{trim($fieldrep->name)}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>	<!-- /controls -->--}}
                                                {{--</div> <!-- /control-group -->--}}


                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Depot :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" id="depot2" name="depotname" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($depots as $depot)
                                                                    <option value="{{$depot->name}}">{{$depot->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Company Name(BDC) :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" > Select</a>

                                                            <select class="form-control" name="bdc" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($bdcs as $bdc)
                                                                    <option value="{{$bdc->name}}">{{$bdc->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->

                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Bank :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" name="bank" required >

                                                                <option selected value="{{$banks}}">{{$banks}}</option>

                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="firstname">Date From</label>
                                                    <div class="controls">
                                                        <input type="date" name="startdate" class="span3" id="datefrom" required>
                                                    </div> <!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="lastname">Date To</label>
                                                    <div class="controls">
                                                        <input type="date" name="enddate" class="span3" id="datefrom" required>
                                                    </div> <!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-primary" style="background-color: darkred">Download PDF</button>
                                                    {{--<button type="submit" class="btn btn-primary" style="background-color: darkred">Download Excel</button>--}}
                                                </div> <!-- /form-actions -->


                                            </fieldset>
                                        </form>
                                    </div>

                                    <div class="tab-pane" id="cummulativestock">
                                        <form id="edit-profile3" class="form-horizontal" action="dailyreportrptbank" method="post">
                                            {{csrf_field()}}
                                            <fieldset>
                                                <input class="form-control" name="typereport" type="hidden" value="3"/>
                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Consignment Name :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" id="consignmentdrop3" name="consignmentname" required>
                                                                <option value="">Select Name</option>
                                                                @foreach($consignments as $consignment)
                                                                    <option value="{{$consignment->consignmentname}}">{{$consignment->consignmentname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->

                                                {{--<div class="control-group">--}}
                                                    {{--<label class="control-label" for="radiobtns">Supervisor :</label>--}}

                                                    {{--<div class="controls">--}}
                                                        {{--<div class="btn-group">--}}
                                                            {{--<a class="btn btn-default" href="#"> Select</a>--}}
                                                            {{--<select class="form-control" name="supervisor" required>--}}
                                                                {{--<option value="">Select Option</option>--}}
                                                                {{--@foreach($supervisorrep as $s)--}}
                                                                    {{--<option value="{{$s->name}}">{{$s->name}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>	<!-- /controls -->--}}
                                                {{--</div> <!-- /control-group -->--}}


                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Depot :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" id="depot3" name="depotname" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($depots as $depot)
                                                                    <option value="{{$depot->name}}">{{$depot->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Company Name(BDC) :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" name="bdc" required>
                                                                <option value="">Select Option</option>
                                                                @foreach($bdcs as $bdc)
                                                                    <option value="{{$bdc->name}}">{{$bdc->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->

                                                <div class="control-group">
                                                    <label class="control-label" for="radiobtns">Bank :</label>

                                                    <div class="controls">
                                                        <div class="btn-group">
                                                            <a class="btn btn-default" href="#"> Select</a>

                                                            <select class="form-control" name="bank" required>

                                                                <option selected value="{{$banks}}">{{$banks}}</option>

                                                            </select>
                                                        </div>
                                                    </div>	<!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="firstname">Date From</label>
                                                    <div class="controls">
                                                        <input type="date" name="startdate" class="span3" id="datefrom" required>
                                                    </div> <!-- /controls -->
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="lastname">Date To</label>
                                                    <div class="controls">
                                                        <input type="date" name="enddate" class="span3" id="datefrom" required>
                                                    </div> <!-- /controls -->
                                                </div> <!-- /control-group -->

                                                <br />


                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-primary" style="background-color: darkred">Download PDF</button>
                                                    {{--<button type="submit" class="btn btn-primary" style="background-color: darkred">Download Excel</button>--}}
                                                </div> <!-- /form-actions -->
                                            </fieldset>
                                        </form>
                                    </div>

                                </div>


                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->


</div>
<!-- /main -->


<!-- /footer -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="asset/js/jquery-1.7.2.min.js"></script>
<script src="asset/js/bootstrap.js"></script>
<script>
    $(document).on('change','#consignmentdrop1,#consignmentdrop2,#consignmentdrop3',function() {

        var consignmentname=this.value;
        $.ajax({
            type: 'POST',
            url: '{{URL::to('getRelatedDepotName')}}',
            data: {
                '_token': "{{ csrf_token() }}",
                'consignmentname':consignmentname
            },
            success: function (result) {
                var data = JSON.parse(result);

                 $('#depot1,#depot2,#depot3').empty();

                 var $select=$('#depot1,#depot2,#depot3');

                 $.each(data, function (index, item) {
                 $select.append(

                   $('<option value="'+data[index].depot+'">'+data[index].depot+'</option>'))
                  }
                 )
            }
        })


    });
</script>
</body>
</html>
