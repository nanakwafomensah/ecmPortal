<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
@include('layout.admin.navbarToplayout')
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget " >

                        <div class="widget-header" style="background: white !important; ">

                            <h3 style="color:darkred !important;">Bulk Distribution Oil Companies records</h3>
                        </div>
                    </div>
                    <button class="btn btn-lg" style="background-color:darkred;color: #fff" data-toggle="modal" data-target="#addBdcModal">Add New</button><span>&nbsp;</span>


                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Company Name</th>
                            <th>Address</th>

                            <th>PhoneNumber</th>
                            <th>Bank</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Company Name</th>
                            <th>Address</th>
                            <th>PhoneNumber</th>
                            <th>Bank</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                       <?php
                          $i=1;
                       ?>
                        @foreach($bdcs as $bdc )
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$bdc->name}}</td>
                            <td>{{$bdc->address}}</td>
                            <td>{{$bdc->phonenumber}}</td>
                            <th>{{$bdc->bankname}}</th>
                            <td>{{$bdc->created_at}}</td>
                            <td>{{$bdc->updated_at}}</td>
                            <td>
                                <div class="controls">
                                    <div class="btn-group">
                                        <a class="btn " style="background-color:white;color: darkred" href="#"><i class="icon-action icon-white"></i>Select</a>
                                        <a class="btn  dropdown-toggle" style="background-color:darkred;color: #fff" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a data-toggle="modal"  data-target="#editBdcModal" class="editbtn"  data-id="{{$bdc->id}}" data-name="{{$bdc->name}}" data-address="{{$bdc->address}}" data-phonenumber="{{$bdc->phonenumber}}" data-bankname="{{$bdc->bankname}}"><i class="icon-pencil"></i> Edit</a></li>
                                            <li><a class="deletebtn"  data-toggle="modal"  data-target="#deleteBdcModal" data-id="{{$bdc->id}}" data-name="{{$bdc->name}}"><i class="icon-trash"></i> Delete</a></li>
                                         </ul>
                                    </div>
                                </div>	<!-- /controls -->
                            </td>

                        </tr>
                       @endforeach
                        </tbody>
                    </table>


                </div> <!-- /widget -->

                </div> <!-- /span8 -->




            <!-- Modal -->
            <div id="addBdcModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">New BDC</h4>
                        </div>
                        <form action="savebdc" method="post">
                        <div class="modal-body">

                            <div class="control-group">
                                <label class="control-label" for="firstname">Company Name:</label>
                                <div class="controls">
                                    <input type="text" class="span5" id="name" name="name"  required>
                                </div> <!-- /controls -->
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="firstname">Address:</label>
                                <div class="controls">
                                    <input type="text" class="span5" id="address" name="address" required >
                                </div> <!-- /controls -->
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="firstname">Phone Number:</label>
                                <div class="controls">
                                    <input type="text" class="span5" id="phonenumber" name="phonenumber" required>
                                </div> <!-- /controls -->
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="radiobtns">Select Bank:</label>

                                <div class="controls">
                                    <select name="bankname" id="name" class="span5">
                                        <option value="">Select An Option</option>
                                        @foreach($banks as $bank)
                                            <option >{{$bank->name}}</option>
                                        @endforeach
                                    </select>
                                </div>	<!-- /controls -->
                            </div> <!-- /control-group -->


                        </div>
                        <div class="modal-footer form-actions">
                            {{csrf_field()}}
                            <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        </form>
                    </div>

                </div>
            </div>
             <!-- Modal -->
            <div id="editBdcModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit BDC</h4>
                        </div>
                        <form action="updatebdc" method="post">
                            <div class="modal-body">
                               <input type="hidden" id="bdc_edit_id" name="idedit" />
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Company Name</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="name_edit" name="nameedit"  required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Address</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="address_edit" name="addressedit" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Phone Number</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="phonenumber_edit" name="phonenumberedit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">Select Bank</label>

                                    <div class="controls">
                                        {{--<select name="banknameedit" id="bankname_edit">--}}
                                        <select class="span5" name="banknameedit" id="bankname_edit">
                                            <option value="">Select An Option</option>
                                            @foreach($banks as $bank)
                                                <option >{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>	<!-- /controls -->
                                </div> <!-- /control-group -->


                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save Changes</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="deleteBdcModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete BDC</h4>
                        </div>
                        <form action="deletebdc" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="bdc_delete_id" name="iddelete" />

                               <p>Are you sure u wnt to delete <span id="nameOfBdc"></span></p>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Delete</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->







<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="asset/js/toastr.min.js"></script>
<script src="asset/js/utility.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('click','.editbtn',function() {

        $('#bdc_edit_id').val($(this).data('id'));
        $('#name_edit').val($(this).data('name'));
        $('#address_edit').val($(this).data('address'));
        $('#phonenumber_edit').val($(this).data('phonenumber'));
        $('#bankname_edit').val($(this).data('bankname')).change();


    });
    $(document).on('click','.deletebtn',function() {
           $('#bdc_id_delete').val($(this).data('id'));
           $('#nameOfBdc').html($(this).data('name'));
     });
</script>
</body>

</html>
