<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Sentinel;
class BankreprecordController extends Controller
{
    //
    //
    public function index(){

        $dailyrecord=DB::table('deportdatas')->where('status','valid')->get();
        return view("bank.bankreprecord")->with(['dailyrecord'=>$dailyrecord]);
    }

}
