<?php

namespace App\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use DB;
class ApiController extends Controller
{
    //
    public function authenticateUser(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        Sentinel::authenticate($credentials);
        if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'Fieldrep') {

            $name=Sentinel::getUser()->first_name . ' '. Sentinel::getUser()->last_name;
           

            $userAssignedcon=Sentinel::getUser()->consignmentname;
            $userAssignedconstant=array();
            $userAssignedcons=explode(',',$userAssignedcon);
            foreach($userAssignedcons as $i){
                $constant = DB::table('contants')->where('consignmentid',$i)->get()->first();
                array_push($userAssignedconstant, $constant);
            }

            $dailyrecord=DB::table('deportdatas')->where('user',$name)->get();

              return response()->json([
                     'fieldrepname' => $name,
                     'status' => '1',
                     'userAssighe' => $userAssignedconstant,
                     'record' => $dailyrecord
                ]);

        }

    }

    

}
