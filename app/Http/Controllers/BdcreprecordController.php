<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Sentinel;
class BdcreprecordController extends Controller
{
    //
    //

    public function index(){

        $dailyrecord=DB::table('deportdatas')->where('status','valid')->get();
        return view("bdc.bdcreprecord")->with(['dailyrecord'=>$dailyrecord]);
    }

}
