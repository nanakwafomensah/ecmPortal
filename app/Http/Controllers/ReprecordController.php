<?php

namespace App\Http\Controllers;
use App\Deportdata;
use Illuminate\Http\Request;
use DB;
use Sentinel;


class ReprecordController extends Controller
{
    //
    public function index(){
      
        $dailyrecord=DB::table('deportdatas')->where('status','invalid')->get();
          return view("admin/reprecord")->with(['dailyrecord'=>$dailyrecord]);
    }


    public function update(Request $request){
        $obs=$request->obslitreedit;
        $vcf=$request->vcfedit;
        $density=$request->densityedit;
        try{
            $m = Deportdata::findorfail($request->idedit);
          //  $m->user=Sentinel::getUser()->first_name . ' '. Sentinel::getUser()->last_name;
            $m->vehiclenumber=$request->vehiclenumberedit;
            $m->drivername=$request->drivernameedit;
            $m->shoretank=$request->shoretankedit;
            $m->orderno=$request->ordernumberedit;
            $m->waybillno=$request->waybillnumberedit;
            $m->customer=$request->customeredit;
            $m->obslitre=$request->obslitreedit;

            $m->tempdeg=$request->temdegedit;
            $m->density=$request->densityedit;
            $m->vcf=$request->vcfedit;
            $m->status='valid';

            $m->litre=$this->getLiter($obs,$vcf);
            $m->vac=$this->getVac($density,$obs,$vcf);
            $m->tonmetricair=$this->gettonMetric($density,$obs,$vcf);
            // dd($m);
            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('reprecord')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('reprecord')->with($notification);
        }
    }
    public function delete(Request $request){
        try{
            $m = Deportdata::findorfail($request->iddelete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Record has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('reprecord')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('reprecord')->with($notification);
        }
    }
    private function getLiter($obs,$vcf){
        $obs=(double)$obs;
        $vcf=(double)$vcf;
        return round($obs *$vcf);
    }
    private function getVac($density,$obs,$vcf){
        $obs=(double)$obs;
        $vcf=(double)$vcf;
        $density=(double)$density;
        return  ($density * $obs * $vcf ) /1000;
    }
    private function gettonMetric($density,$obs,$vcf){
        $obs=(double)$obs;
        $vcf=(double)$vcf;
        $density=(double)$density;
        return  (($obs * $vcf)/1000)*( $density - 0.0011);
    }

}
