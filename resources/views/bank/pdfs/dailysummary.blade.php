<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Daily Summary report</title>

    <style>
        #datarecord,#brieftable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #ff8533;
            color: black;
            font-size: 12px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }

    </style>
</head>
<body >
<div class="container" >
    <div class="page">

        <h4><center><b>SUMMARY REPORT</b></center></h4>
        <center> <img style="float: right" src="img/ecm_logo.jpg" width="5px" height="3px" alt="logo" /></center>

        <img style="float: right;" src="img/ecm_logo.jpg" width="110px" height="40px" alt="logo" />
        <table id="brieftable" style="border: 1px solid black; width: 100%; " >
            <tr >
                <td>DATE :</td>
                <td><b>{{date('d-m-Y')}}</b></td>

            </tr>
            <tr >
                <td>COMPANY NAME :</td>
                <td><b>{{$bdc}}</b></td>

            </tr>
            <tr>
                <td>DEPOT NAME :</td>
                <td><b>{{$depotname}}</b></td>

            </tr>
            <tr>
                <td>DEPOT LOCATION :</td>
                <td><b>{{$depotlocation}}</b></td>

            </tr>
            <tr>
                <td>PRODUCT :</td>
                <td><b>{{$product}}</b></td>

            </tr>
            <tr>
                <td></td>
                <td></td>

            </tr>
            <tr>
                <td></td>
                <td style="border-right: 1px solid black;" ></td>

            </tr>
            <tr>
                <td>BANK :</td>
                <td style="border-right: 1px solid black;"><b>{{$bank}}</b></td>

            </tr>
        </table>
        <br>
        <table id="brieftable" style="border: 1px solid black; width: 100%; ">
            <tr style="background-color:#ff8533">
                <td style="border-right: 1px solid black;"><center>DEPOTS :</center></td>
                <td ><center><b>{{$depotname}}</b></center></td>
                <td></td>
            </tr>
            <tr style="background-color:#ff8533">
                <td style="border-right: 1px solid black;"><center>PRODUCTS :</center></td>
                <td><center><b>{{$product}}</b></center></td>
                <td></td>
            </tr>
            <tr style="background-color:#ff8533">
                <td style="border-right: 1px solid black;"><center>TOTAL QUANTITY(MT AIR) :</center></td>
                <td  ><center><b>{{$mainconsignment}}</b></center></td>
                <td></td>
            </tr>
            <tr style="background-color:#ff8533">
                <td style="border-right: 1px solid black;"><center>TOTAL QUANTITY(GROSS LITRES):</center></td>
                <td ><center><b>{{$totalquantity_in_Gross_litres}}</b></center></td>
                <td></td>
            </tr>
            <tr style="background-color:#ff8533">
                <td style="border-right: 1px solid black;"><center>UNRELEASED QUANTITY(MT AIR) :</center></td>
                <td  ><center><b>{{$mainconsignment - $initialConsignmentInfo->totalquantity}}</b></center></td>
                <td></td>
            </tr>
            <tr style="background-color:#ff8533">
                <td style="border-right: 1px solid black;" ><center>RELEASED QUANTITY(MT AIR) :</center></td>
                <td ><center><b>{{$initialConsignmentInfo->totalquantity}}</b></center></td>
                <td></td>
            </tr>
            <thead>
            <tr style="background-color:#ff8533">
                <td style="border-right: 1px solid black;"><center><b>DATE</b></center></td>
                <td style="border-right: 1px solid black;" ><center><b>QUANTITY LOADED</b></center></td>
                <td style="border-right: 1px solid black;" ><center><b>QUANTITY REMAINING</b></center></td>
            </tr>
            </thead>
            <tbody id="#brieftable">

            <?php
            $remainder = $initialConsignmentInfo->totalquantity;
            $totalquantityloaded=0;
            $totalRemainder=0;
            ?>
            @foreach($records as $r)
                <?php
                $actualremainder=(int)$remainder - (int)$r->quantityloaded;
                $totalquantityloaded = $totalquantityloaded + $r->quantityloaded;
                $totalRemainder = $totalRemainder + $actualremainder;
                ?>

            <tr>
                <td style="border-right: 1px solid black;" ><center>{{$r->date}}</center></td>
                <td style="border-right: 1px solid black;" ><center>{{sprintf('%0.3f', $r->quantityloaded)}}</center></td>
                <td style="border-right: 1px solid black;" ><center>{{sprintf('%0.3f', $actualremainder)}}</center></td>
            </tr>
                <?php $remainder = (int)$remainder - (int)$r->quantityloaded ;?>
            @endforeach
            <tr>
                <td style="border-right: 1px solid black;" colspan="3"></td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td style="border-right: 1px solid black;" ><b><center>{{"TOTAL"}}</center></b></td>
                <td style="border-right: 1px solid black;" ><b><center>{{sprintf('%0.3f', $totalquantityloaded) }}</center></b></td>
                <td style="border-right: 1px solid black;" ><b><center>{{sprintf('%0.3f', $actualremainder)}}</center></b></td>
            </tr>
            </tfoot>
        </table>
    </div>
  <div class="page">

      <table id="brieftable" style="border: 1px solid black; width: 100%; " >
          <tr >
              <td>DATE :</td>
              <td><b>{{date('d-m-Y')}}</b></td>

          </tr>
          <tr >
              <td>COMPANY NAME :</td>
              <td><b>{{$bdc}}</b></td>

          </tr>
          <tr>
              <td>DEPOT NAME :</td>
              <td><b>{{$depotname}}</b></td>

          </tr>
          <tr>
              <td>DEPOT LOCATION :</td>
              <td><b>{{$depotlocation}}</b></td>

          </tr>
          <tr>
              <td>PRODUCT :</td>
              <td><b>{{$product}}</b></td>

          </tr>
          <tr>
              <td></td>
              <td></td>

          </tr>
          <tr>
              <td></td>
              <td style="border-right: 1px solid black;" ><b></b></td>

          </tr>
          <tr>
              <td>BANK :</td>
              <td style="border-right: 1px solid black;"><b>{{$bank}}</b></td>

          </tr>
      </table>
      <br>
      <h4>IMPORTS AND IN-TANK PURCHASES AND RELEASES AT  {{$product}}</h4>

      <table id="datarecord" style="border: 1px solid black; width: 100%; ">
          <tr>
              <th>PRODUCT</th>
              <th>TERMINALS</th>
              <th>GROSS METRIC TONS IN AIR</th>
              <th>GROSS LITRES IN TANK</th>
              <th>DATE OF RELEASE</th>
              <th>QUANTITY RELEASED/METRIC TONS</th>
              <th>UNRELEASED QUANTITY/METRIC TONS</th>
          </tr>
          <tr>
              <td rowspan="2">{{$product}}</td>
              <td rowspan="2">{{$depotname}}</td>
              <td>{{$initialConsignmentInfo->totalquantity}}</td>
              <td>{{$totalquantity_in_Gross_litres}}</td>
              <td>{{$initialConsignmentInfo->date}}</td>
              <td>{{$initialConsignmentInfo->totalquantity}}</td>
              <td>{{"0.00"}}</td>

          </tr>
          <tr>

              <td>{{$mainconsignment - $initialConsignmentInfo->totalquantity}}</td>
              <td></td>
              <td></td>
              <td>{{"0.00"}}</td>
              <td>{{$initialConsignmentInfo->totalquantity}}</td>
          </tr>
      </table>
  </div>
</div>
</body>
</html>