<?php

namespace App\Http\Controllers;

use App\Consignment;
use App\Contant;
use App\Mainconsignment;
use Illuminate\Http\Request;
use DB;
use Sentinel;
class MainConsignmentController extends Controller
{
    //
    public function index(){
        $bdcs=DB::table('ddcs')->get();
        $banks=DB::table('banks')->get();
        $depots=DB::table('depots')->get();
        $mainconsignments=DB::table('mainconsignments')->get();

        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();

        return view('admin.entry.mainconsignment')->with([
            'bdcs'=>$bdcs ,
            'banks'=>$banks,
            'depots'=>$depots,
            'mainconsignments'=>$mainconsignments,
            'constants'=>$constants,
            'consignment'=>$consignment

        ]);
    }

    public function save(Request $request){

        try{
            $m = new Mainconsignment();
            $m->consignmentname=$request->consignmentname;
            $m->quantity=$request->quantity;
            $m->description=$request->description;
            $m->bank=$request->bank;
           
           if($m->save()){
                $notification=array(
                    'message'=>"New Bank Consigment  has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('mainconsignment')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('mainconsignment')->with($notification);
        }
    }

    public function update(Request $request){
        try{
            $m = Mainconsignment::findorfail($request->idedit);
            $m->consignmentname=$request->consignmentname_edit;
            $m->quantity=$request->totalquantity_edit;
            $m->description=$request->description_edit;
            $m->bank=$request->bank_edit;
           

            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('mainconsignment')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Consigment  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('mainconsignment')->with($notification);
        }
    }
    public function delete(Request $request){

        try{
            $m = Mainconsignment::findorfail($request->iddelete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Consigment has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('mainconsignment')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('mainconsignment')->with($notification);
        }
    }
}
