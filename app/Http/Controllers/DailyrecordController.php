<?php

namespace App\Http\Controllers;
use Redirect;
use Session;
use App\Deportdata;
use Illuminate\Http\Request;
use DB;
use Sentinel;


class DailyrecordController extends Controller
{
    public function index(){
        $name=Sentinel::getUser()->first_name . ' '. Sentinel::getUser()->last_name;
        $userAssignedcon=Sentinel::getUser()->consignmentname;
        $userAssignedconstant=array();
        $userAssignedcons=explode(',',$userAssignedcon);
        foreach($userAssignedcons as $i){
            $constant = DB::table('contants')->where('consignmentid',$i)->get()->first();
            array_push($userAssignedconstant, $constant);
        }
        $dailyrecord=DB::table('deportdatas')->where('user',$name)->orderBy('created_at','DESC')->get();

        return view("fieldrep.dailyrecord")->with(['dailyrecord'=>$dailyrecord,'constant'=>$userAssignedconstant]);
    }
    public function save(Request $request){
        $obs=$request->obslitre;
        $vcf=$request->vcf;
        $density=$request->density;
        $assignedSupervisors=DB::table('users')->join('role_users', 'users.id', '=', 'role_users.user_id')->select('name')->where('depot',Sentinel::getUser()->depot)->where('role_users.role_id','3')->get();
        $listOfAssignedsupervisors=array();
        foreach ($assignedSupervisors  as $i){
             array_push($listOfAssignedsupervisors,$i->name);
        }

       if(empty($listOfAssignedsupervisors)){
          Session::flash('message', "Ooops No Supervisor has been assigned that consignment .Contact Administrator");
          return Redirect::back();
       }
        try{
            $m = new Deportdata();
            $m->user=Sentinel::getUser()->first_name . ' '. Sentinel::getUser()->last_name;
            $m->supervisor=implode(',',$listOfAssignedsupervisors);
            $m->vehiclenumber=$request->vehiclenumber;
            $m->consignmentname=$request->consignmentname;
            $m->drivername=$request->drivername;
            $m->shoretank=$request->shoretank;
            $m->orderno=$request->ordernumber;
            $m->waybillno=$request->waybillnumber;
            $m->customer=$request->customer;
            $m->obslitre=$request->obslitre;
            $m->tempdeg=$request->temdeg;
            $m->density=$request->density;
            $m->vcf=$request->vcf;
            $m->litre=$this->getLiter($obs,$vcf);
            $m->vac=$this->getVac($density,$obs,$vcf);
            $m->tonmetricair=$this->gettonMetric($density,$obs,$vcf);
            $m->status='invalid';

            $m->depotname=Sentinel::getUser()->depot;
            $m->bank=Sentinel::getUser()->bank;
            $m->bdc=Sentinel::getUser()->bdc;

            if($m->save()){
                $notification=array(
                    'message'=>"New Record  has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('dailyrecord')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('dailyrecord')->with($notification);
        }
    }
    public function update(Request $request){
        $obs=$request->obslitreedit;
        $vcf=$request->vcfedit;
        $density=$request->densityedit;
        try{
            $m = Deportdata::findorfail($request->idedit);
            $m->user=Sentinel::getUser()->first_name . ' '. Sentinel::getUser()->last_name;
            $m->consignmentname=$request->consignmentnameedit;
            $m->vehiclenumber=$request->vehiclenumberedit;
            $m->drivername=$request->drivernameedit;
            $m->shoretank=$request->shoretankedit;
            $m->orderno=$request->ordernumberedit;
            $m->waybillno=$request->waybillnumberedit;
            $m->customer=$request->customeredit;
            $m->obslitre=$request->obslitreedit;
            $m->tempdeg=$request->temdegedit;
            $m->density=$request->densityedit;
            $m->vcf=$request->vcfedit;
            $m->status='invalid';
            $m->litre=$this->getLiter($obs,$vcf);
            $m->vac=$this->getVac($density,$obs,$vcf);
            $m->tonmetricair=$this->gettonMetric($density,$obs,$vcf);

            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('dailyrecord')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('dailyrecord')->with($notification);
        }
    }
    public function delete(Request $request){
        try{
            $m = Deportdata::findorfail($request->iddelete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Record has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('dailyrecord')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('dailyrecord')->with($notification);
        }
    }
    private function getLiter($obs,$vcf){
        $obs=(double)$obs;
        $vcf=(double)$vcf;
        return round($obs *$vcf);
    }
    private function getVac($density,$obs,$vcf){
        $obs=(double)$obs;
        $vcf=(double)$vcf;
        $density=(double)$density;
        return  ($density * $obs * $vcf ) /1000;
    }
    private function gettonMetric($density,$obs,$vcf){
        $obs=(double)$obs;
        $vcf=(double)$vcf;
        $density=(double)$density;
        return  (($obs * $vcf)/1000)*( $density - 0.0011);
    }


}
