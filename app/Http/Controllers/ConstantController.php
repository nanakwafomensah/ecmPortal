<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Contant;
class ConstantController extends Controller
{
    public function index(){

        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();

        $consignments=DB::table('consignments')->get();
        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();
        
        return view("admin/entry/constant")->with([
            'consignments'=>$consignments,
            'constants'=>$constants,
            'consignment'=>$consignment
        ]);
    }
  

    public function update(Request $request){

        try{
            $m = Contant::where('consignmentid', $request->consignmentname)->first();
            $m->temDeg=$request->temDeg;
            $m->density=$request->density;
            $m->vcf=$request->vcf;
           


            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('constant')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('constant')->with($notification);
        }
    }
    
    
    public function getConstant(Request $request){

        $constants=DB::table('contants')->where('consignmentid',trim($request->consignmentname))->get()->first();
       // dd($constants);
        return json_encode($constants);
        

    }
}
