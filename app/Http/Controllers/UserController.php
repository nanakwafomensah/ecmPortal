<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Image;
use DB;


class UserController extends Controller
{
    //
    
    public function index(){
        $userdetails = DB::table('users')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'roles.id', '=', 'role_users.role_id')
            ->select('users.id','users.first_name','users.last_name','users.email','roles.name as priviledge','users.last_login' ,'users.depot','users.bank','users.bdc','users.consignmentname')
            ->get();
        
        $consignments= DB::table('consignments')->where('status',"open")->get();

        $depots = DB::table('depots')->get();
        $bdcs=DB::table('ddcs')->get();
        $banks = DB::table('banks')->get();
        $constants = DB::table('consignments')
            ->join('contants', 'consignments.consignmentname', '=', 'contants.consignmentid')
            ->select('contants.*', 'consignments.*')->where('status','open')
            ->first();

        $consignment= DB::table('consignments')
            ->join('deportdatas', 'consignments.consignmentname', '=', 'deportdatas.consignmentname')
            ->select( DB::raw('count(deportdatas.id) as totalrecorded'))
            ->groupBy('consignments.consignmentname','consignments.id', 'consignments.reference_number', 'consignments.supplier', 'consignments.date_issued', 'consignments.date_expire', 'consignments.totalquantity', 'consignments.description', 'consignments.port_of_discharge', 'consignments.product', 'consignments.bank', 'consignments.depot', 'consignments.bdc', 'consignments.contact', 'consignments.status', 'consignments.created_at', 'consignments.updated_at')
            ->where('consignments.status','open')->first();
        return view("admin/entry/user")->with([
            'userdetails'=>$userdetails,
            'depots'=>$depots,
            'banks'=>$banks,
            'consignments'=>$consignments,
            'bdcs'=>$bdcs,
            'constants'=>$constants,
            'consignment'=>$consignment
        ]);
    }

    public function adduser(Request $request){
       // dd($request->consignmentname);
        $credentials = [
            'email'    => $request->email,
            'password' => $request->password,
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'tel' => $request->tel,
            'name' => $request->firstname .' '.$request->lastname,
            'depot' => $request->depot,
            'consignmentname' => $request->consignmentname == '' ? null: implode(",",$request->consignmentname),
            'bank' => $request->bank,
            'bdc' => $request->bdc,
        ];
 
        $user = Sentinel::registerAndActivate($credentials);
        $role=Sentinel::findRoleBySlug($request->role);
        $role->users()->attach($user);
        $notification=array(
            'message'=>"$request->firstname has been Succesfully added",
            'alert-type'=>'success'
        );
        return redirect('user')->with($notification);
    }
    public function edituser(Request $request){

        $previousrole = Sentinel::findById($request->id_edit)->roles()->first();//get previous role of user
         $user = Sentinel::findById($request->id_edit);               ////////////////////
        $role = Sentinel::findRoleByName($previousrole->name);       /////Remove user role////////////////
        $role->users()->detach($user);                               ////////////////


        $user1 = Sentinel::findById($request->id_edit);
        Sentinel::update($user1, array(
            'first_name' => $request->firstname_edit,
            'last_name' => $request->lastname_edit,
            'password' => $request->password_edit,
            'email' => $request->email_edit,
            'depot' => $request->depotedit,
            'bank' => $request->editbank,
            'bdc' => $request->editbdc,
            'consignmentname' => $request->consignmentnameedit == '' ? null: implode(",",$request->consignmentnameedit),
        ));

        $role_edit=Sentinel::findRoleBySlug($request->role_edit);
        $role_edit->users()->attach($user);

        $notification=array(
            'message'=>"User details has been Succesfully updated",
            'alert-type'=>'success'
        );
        return redirect('user')->with($notification);



    }

    public function deleteuser(Request $request){

        $user = Sentinel::findById($request->id_delete);

        if($user->delete()){
            $notification=array(
                'message'=>"user has been deleted",
                'alert-type'=>'success'
            );
            return redirect('user')->with($notification);
        }

    }
}
