<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ecm-portal</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />
 <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
@include('layout.admin.navbarToplayout')
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header" style="background: white !important; ">

                            <h3 style="color:darkred;">CONSIGNMENT RELEASE</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->
                    <button id="modalshowcon" class="btn btn-lg" style="background-color:darkred;color: #fff" data-toggle="modal" data-target="#addbankModal">Add New</button><span>&nbsp;</span>


                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Consignment Name</th>
                            <th>Letter Of Credit Number</th>
                            <th>Supplier</th>
                            <th>Port Of Discharge</th>
                            <th>Description</th>
                            <th>Product</th>

                            <th>Date Issued</th>
                            <th>Date Expire</th>
                            <th>Total Quantity</th>
                            <th>Action</th>
                            <th>Status</th>
                            <th>Switch Status</th>

                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Consignment Name</th>
                            <th>Letter Of Credit Number</th>
                            <th>Supplier</th>
                            <th>Port Of Discharge</th>
                            <th>Description</th>
                            <th>Product</th>

                            <th>Date Issued</th>
                            <th>Date Expire</th>
                            <th>Total Quantity</th>
                            <th>Action</th>
                            <th>Status</th>
                            <th>Switch Status</th>

                        </tr>
                        </tfoot>
                        <?php
                        $i=1;
                        ?>
                        @foreach($consignments as $consignment)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$consignment->consignmentname}}</td>
                                <td>{{$consignment->reference_number}}</td>
                                <td>{{$consignment->supplier}}</td>
                                <td>{{$consignment->port_of_discharge}}</td>
                                <td>{{$consignment->description}}</td>
                                <td>{{$consignment->product}}</td>
                                <td>{{$consignment->date_issued}}</td>
                                <td>{{$consignment->date_expire}}</td>
                                <td>{{$consignment->totalquantity}}</td>
                                <td>
                                    <div class="controls">
                                        <div class="btn-group">
                                            <a class="btn " style="background-color:white;color: darkred" href="#"><i class="icon-action icon-white"></i>Select</a>
                                            <a class="btn  dropdown-toggle" style="background-color:darkred;color: #fff" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-toggle="modal"  data-target="#editbankModal" class="editbtn"
                                                       data-id="{{$consignment->id}}"
                                                       data-reference_number="{{$consignment->reference_number}}"
                                                       data-supplier="{{$consignment->supplier}}"
                                                       data-date_issued="{{$consignment->date_issued}}"
                                                       data-date_expire="{{$consignment->date_expire}}"
                                                       data-totalquantity="{{$consignment->totalquantity}}"
                                                       data-description="{{$consignment->description}}"
                                                       data-port_of_discharge="{{$consignment->port_of_discharge}}"
                                                       data-product="{{$consignment->product}}"
                                                       data-depot="{{$consignment->depot}}"
                                                       data-bank="{{$consignment->bank}}"
                                                       data-bdc="{{$consignment->bdc}}"
                                                       data-consignmentname="{{$consignment->consignmentname}}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a data-toggle="modal"  data-target="#deletebankModal" class="deletebtn" data-id="{{$consignment->id}}"  data-consignmentname="{{$consignment->consignmentname}}" data-reference_number="{{$consignment->reference_number}}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>	<!-- /controls -->
                                </td>
                                <td>{{$consignment->status}}</td>
                                <td><a href="switchconsignmentstate/{{$consignment->id}}/{{$consignment->status}}" class="btn btn-default"> Switch</a></td>
                                      </tr>
                            @endforeach
                            </tbody>
                    </table>


                </div> <!-- /widget -->

            </div> <!-- /span8 -->



            <!-- Modal -->
            <div id="addbankModal" class="modal fade" >
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">NEW CONSIGNMENT RELEASE</h4>
                        </div>
                        <form action="saveconsignment" method="post">
                            <div class="modal-body">

                                             <div class="control-group">
                                                 <label class="control-label" for="firstname">Consignment Name</label>
                                                 <div class="controls">
                                                     <select name="consignmentname" id="consignmentname" class="span5">
                                                         <option value="">Select A Consignment</option>
                                                         @foreach($mainconsignments as $con)
                                                             <option value="{{$con->consignmentname}}">{{$con->consignmentname}}</option>
                                                        @endforeach
                                                     </select>

                                                 </div>
                                             </div>


                                             <div class="control-group">
                                                 <label class="control-label" for="firstname">Contact</label>
                                                 <div class="controls">
                                                     <input type="text" class="span5" id="contact" name="contact" placeholder="0268989899"  required>
                                                 </div> <!-- /controls -->
                                             </div>







                                <div class="control-group">
                                    <label class="control-label" for="firstname">Letter of Credit Number</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="name" name="referenceNumber"  required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Name of Supplier</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="address" name="supplier" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Date Issued</label>
                                    <div class="controls">
                                        <input type="date" class="span5" id="phonenumber" name="date_issued" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Date Expire</label>
                                    <div class="controls">
                                        <input type="date" class="span5" id="phonenumber" name="date_expire" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Total quantity</label>
                                    <div class="controls">
                                        <input type="number" step="any"  class="span5" id="phonenumber" name="totalquantity" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Description</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="phonenumber" name="description" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Port Of Discharge</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="phonenumber" name="port_of_discharge" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Name of product</label>
                                    <div class="controls">
                                        <select name="product">

                                                <option value="AGO">AGO</option>
                                                <option value="PMS">PMS</option>
                                                <option value="RFO">RFO</option>
                                                <option value="ATK">ATK</option>

                                        </select>

                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Depot Assigned</label>
                                    <div class="controls">
                                        <select name="depot">
                                            @foreach($depots as $depot)
                                                <option value="{{$depot->name}}">{{$depot->name}}</option>
                                            @endforeach
                                        </select>

                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Bank financed</label>
                                    <div class="controls">
                                        <select name="bank">
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->name}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Name of BDC</label>
                                    <div class="controls">
                                        <select name="bdc">
                                            @foreach($bdcs as $bdc)
                                                <option value="{{$bdc->name}}">{{$bdc->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> <!-- /controls -->
                                </div>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <!-- Modal -->
            <div id="editbankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">EDIT CONSIGNMENT RELEASE</h4>
                        </div>
                        <form action="updateconsignment" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="bank_edit_id" name="idedit" />
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Consignment Name</label>
                                    <div class="controls">
                                        <select name="consignmentname_edit" id="consignmentname_edit" class="span5">
                                            <option value="">Select A Consignment</option>
                                            @foreach($mainconsignments as $con)
                                                <option value="{{$con->consignmentname}}">{{$con->consignmentname}}</option>
                                            @endforeach
                                        </select>

                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Letter of Credit Number</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="referenceNumber_edit" name="referenceNumber_edit"  required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Name of Supplier</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="supplier_edit" name="supplier_edit" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Date Issued</label>
                                    <div class="controls">
                                        <input type="date" class="span5" id="date_issued_edit" name="date_issued_edit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Date Expire</label>
                                    <div class="controls">
                                        <input type="date" class="span5" id="date_expire_edit" name="date_expire_edit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Total quantity</label>
                                    <div class="controls">
                                        <input type="number" step="any"  class="span5" id="totalquantity_edit" name="totalquantity_edit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Description</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="description_edit" name="description_edit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Port Of Discharge</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="port_of_discharge_edit" name="port_of_discharge_edit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Name of product</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="product_edit" name="product_edit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Depot Assigned</label>
                                    <div class="controls">
                                        <select name="depot_edit" id="depot_edit">
                                            @foreach($depots as $depot)
                                                <option value="{{$depot->name}}">{{$depot->name}}</option>
                                            @endforeach
                                        </select>

                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Bank financed</label>
                                    <div class="controls">
                                        <select name="bank_edit" id="bank_edit">
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->name}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Name of BDC</label>
                                    <div class="controls">
                                        <select name="bdc_edit" id="bdc_edit">
                                            @foreach($bdcs as $bdc)
                                                <option value="{{$bdc->name}}">{{$bdc->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> <!-- /controls -->
                                </div>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save Changes</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="deletebankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">DELETE CONSIGNMENT</h4>
                        </div>
                        <form action="deleteconsignment" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="bank_delete_id" name="iddelete" />

                                <p>Are you sure you want to delete <span id="consignmentnamedelete"></span> Consignment</p>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Delete</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main -->









<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>


<script src="asset/js/toastr.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="asset/js/toastr.min.js"></script>
<script src="asset/js/utility.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('click','.addreleasebtn',function() {
        $('#releaseConsignmentName').html($(this).data('consignmentname'));
        var consignmentid=$(this).data('id');

            $.ajax({
                type: 'GET',
                url: 'loadRelease/'+consignmentid,

                success: function (data) {
                    alert(data);
//                    var obj = JSON.parse(data);
//                    $('#density').val(obj.density);
//                    $('#temDeg').val(obj.temDeg);
//                    $('#vcf').val(obj.vcf);
                }
            })

    });
    $(document).on('click','.editbtn',function() {
      // alert($(this).data('referenceNumber'));
        $('#bank_edit_id').val($(this).data('id'));
        $('#referenceNumber_edit').val($(this).data('reference_number'));
        $('#supplier_edit').val($(this).data('supplier'));
        $('#date_issued_edit').val($(this).data('date_issued'));
        $('#date_expire_edit').val($(this).data('date_expire'));
        $('#totalquantity_edit').val($(this).data('totalquantity'));
        $('#description_edit').val($(this).data('description'));
        $('#port_of_discharge_edit').val($(this).data('port_of_discharge'));
        $('#product_edit').val($(this).data('product')).change();
        $('#depot_edit').val($(this).data('depot')).change();
        $('#bank_edit').val($(this).data('bank')).change();
        $('#bdc_edit').val($(this).data('bdc')).change();
        $('#consignmentname_edit').val($(this).data('consignmentname')).change();


    });
    $(document).on('click','.deletebtn',function() {
        $('#bank_delete_id').val($(this).data('id'));
        $('#consignmentnamedelete').html($(this).data('consignmentname'));
    });

</script>



</body>

</html>
