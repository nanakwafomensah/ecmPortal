<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',['as'=>'login','uses'=>'LoginController@index']);
Route::get('login',['as'=>'login','uses'=>'LoginController@index']);
Route::post('login',['as'=>'login','uses'=>'LoginController@postLogin']);


Route::post('logout',['as'=>'logout','uses'=>'LoginController@postlogout']);




Route::get('dashboard',['as'=>'dashboard','uses'=>'DashboardController@index']);
Route::get('bdc',['as'=>'bdc','uses'=>'BdcController@index']);
Route::get('deport',['as'=>'deport','uses'=>'DeportController@index']);
Route::get('setting',['as'=>'setting','uses'=>'SettingController@index']);
Route::get('bank',['as'=>'bank','uses'=>'BankController@index']);
Route::get('constant',['as'=>'constant','uses'=>'ConstantController@index']);
Route::get('report',['as'=>'report','uses'=>'ReportController@index']);
Route::get('reportbank',['as'=>'reportbank','uses'=>'ReportbankController@index']);
Route::get('reportbdc',['as'=>'reportbdc','uses'=>'ReportbdcController@index']);
Route::get('consignment',['as'=>'consignment','uses'=>'ConsignmentController@index']);
Route::get('bankindex',['as'=>'bankindex','uses'=>'BankdashboardController@index']);
Route::get('bdcindex',['as'=>'bdcindex','uses'=>'BdcdashboardController@index']);
Route::get('reprecord',['as'=>'reprecord','uses'=>'ReprecordController@index']);
Route::get('bankreprecord',['as'=>'bankreprecord','uses'=>'BankreprecordController@index']);
Route::get('bdcreprecord',['as'=>'bdcreprecord','uses'=>'BdcreprecordController@index']);
Route::get('mainconsignment',['as'=>'mainconsignment','uses'=>'MainConsignmentController@index']);


Route::get('dailyrecord',['as'=>'dailyrecord','uses'=>'DailyrecordController@index']);
Route::get('user',['as'=>'user','uses'=>'UserController@index']);

Route::post('updatedailyapprove',['as'=>'updatedailyapprove','uses'=>'ReprecordController@update']);
Route::post('deletedailyapprove',['as'=>'deletedailyapprove','uses'=>'ReprecordController@delete']);


Route::post('savebdc',['as'=>'savebdc','uses'=>'BdcController@save']);
Route::post('updatebdc',['as'=>'updatebdc','uses'=>'BdcController@update']);
Route::post('deletebdc',['as'=>'deletebdc','uses'=>'BdcController@delete']);

Route::post('savemainconsignment',['as'=>'savemainconsignment','uses'=>'MainConsignmentController@save']);
Route::post('updatemainconsignment',['as'=>'updatemainconsignment','uses'=>'MainConsignmentController@update']);
Route::post('deletemainconsignment',['as'=>'deletemainconsignment','uses'=>'MainConsignmentController@delete']);

Route::get('switchconsignmentstate/{id}/{state}',['as'=>'switchconsignmentstate','uses'=> 'ConsignmentController@switchconsignmentstate']);
Route::get('loadRelease/{id}',['as'=>'loadRelease','uses'=> 'ConsignmentController@loadRelease']);
Route::post('saveconsignment',['as'=>'saveconsignment','uses'=>'ConsignmentController@save']);
Route::post('updateconsignment',['as'=>'updateconsignment','uses'=>'ConsignmentController@update']);
Route::post('deleteconsignment',['as'=>'deleteconsignment','uses'=>'ConsignmentController@delete']);


Route::post('savedepot',['as'=>'savedepot','uses'=>'DeportController@save']);
Route::post('updatedepot',['as'=>'updatedepot','uses'=>'DeportController@update']);
Route::post('deletedepot',['as'=>'deletedepot','uses'=>'DeportController@delete']);


Route::post('savebank',['as'=>'savebank','uses'=>'BankController@save']);
Route::post('updatebank',['as'=>'updatebank','uses'=>'BankController@update']);
Route::post('deletebank',['as'=>'deletebank','uses'=>'BankController@delete']);
Route::post('getRelatedDepotName',['as'=>'getRelatedDepotName','uses'=>'BankController@getRelatedDepotName']);



Route::post('updateconstant',['as'=>'updateconstant','uses'=>'ConstantController@update']);
Route::post('getConstant',['as'=>'getConstant','uses'=>'ConstantController@getConstant']);



Route::post('savedailyreport',['as'=>'savedailyreport','uses'=>'DailyrecordController@save']);
Route::post('updatedailyreport',['as'=>'updatedailyreport','uses'=>'DailyrecordController@update']);
Route::post('deletedailyreport',['as'=>'deletedailyreport','uses'=>'DailyrecordController@delete']);

Route::post('SendSmsReminder',['as'=>'SendSmsReminder','uses'=>'UtilityController@SendSmsReminderForTreshold']);

Route::post('adduser',['as'=>'adduser','uses'=>'UserController@adduser']);
Route::post('edituser',['as'=>'edituser','uses'=>'UserController@edituser']);
Route::post('deleteuser',['as'=>'deleteuser','uses'=>'UserController@deleteuser']);

Route::post('dailyreportrpt',['as'=>'dailyreportrpt','uses'=> 'PdfController@dailyreport']);
Route::post('dailyreportrptbank',['as'=>'dailyreportrptbank','uses'=> 'PdfController@dailyreportbank']);
Route::post('dailyreportrptbdc',['as'=>'dailyreportrptbdc','uses'=> 'PdfController@dailyreportbdc']);



////////////bank
Route::get('downloadpdfreportbank/{reference_number}/{bank}',['as'=>'downloadpdfreportbank','uses'=> 'PdfController@downloadpdfreportbank']);
Route::get('downloadExcelreportbank/{type}/{reference_number}/{bank}',['as'=>'downloadExcelreportbank','uses'=> 'ExcelController@downloadExcelreportbank']);

////////////bdc
Route::get('downloadpdfreportbdc/{reference_number}/{bdc}',['as'=>'downloadpdfreportbdc','uses'=> 'PdfController@downloadpdfreportbdc']);
Route::get('downloadExcelreportbdc/{type}/{reference_number}/{bdc}',['as'=>'downloadExcelreportbdc','uses'=> 'ExcelController@downloadExcelreportbdc']);
